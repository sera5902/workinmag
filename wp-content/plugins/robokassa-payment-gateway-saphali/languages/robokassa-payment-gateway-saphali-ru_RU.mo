��          T      �       �   H   �                  #   !  �   E  �  �  y   �     8     G     X  /   m    �                                        Allows you to use Robokassa payment gateway with the WooCommerce plugin. Debug Description Instructions Robokassa Payment Gateway (Saphali) Спасибо за Ваш заказ, пожалуйста, нажмите кнопку ниже, чтобы заплатить. Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Project-Id-Version: Robokassa Payment Gateway (Saphali)
POT-Creation-Date: 2015-12-10 08:36+0300
PO-Revision-Date: 2017-10-04 15:46+0500
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-WPHeader: wc-robokassa.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: ru_RU
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Позволяет использовать платежный шлюз Робокаса с плагином WooCommerce. Отладка Описание Инструкции Платежный шлюз Robokassa (Saphali) <h5>Спасибо, ваш заказ принят, пора перейти к оплате.<br><br>Внимание! Если оплата не проходит через мобильный телефон, то воспользуйтесь банковской картой.  </h5> 