<?php
/**
 * Plugin Name: Email Confirmation
 * Description: Require email confirmation for user registration.
 * Part of the code is base on class EmailConfirmation from Cedric Ruiz
 * ( https://gist.github.com/elclanrs/6516451 )
 */

if ( !class_exists( 'JM_EmailConfirmation' ) ) :
	class JM_EmailConfirmation {
		const PREFIX = 'email-confirmation-';

		public function __construct() {

			if( self::get_setting('email_confirmation', false) ) {
				add_filter( 'noo_register_user_data', array( $this, 'add_redirect_link' ), 10, 2 );
				add_filter( 'noo_registration_errors', array( $this, 'require_confirmation' ), 10, 2 );
				add_filter( 'noo_registration_errors', array( $this, 'bypass_verifying_error' ), 10, 2 );

				add_filter( 'noo_register_error_result', array( $this, 'add_redirect_to_member' ), 99, 2 );

				add_filter( 'noo-member-page-endpoint', array( $this, 'member_page_endpoint' ) );
				add_filter( 'noo-member-not-login-shortcode', array( $this, 'verify_email_shortcode' ), 10, 2 );
			}

			if( is_admin() ) {
				add_action( 'noo_setting_member_fields', array( $this, 'setting_fields') );
			}
		}

		public function add_redirect_to_member( $result = array(), $errors = null ) {
			if( self::get_setting('email_confirmation', false) ) {
				if ( is_wp_error( $errors ) && $errors->get_error_code() == 'email_confirmation' ) {
					$result['redirecturl'] = Noo_Member::get_member_page_url();
				}
			}

			return $result;
		}

		public function add_redirect_link( $user_args = array(), $POST = array() ) {
			if( self::get_setting('email_confirmation', false) ) {
				$redirect_to = '';
				if( $user_args['role'] == Noo_Member::CANDIDATE_ROLE )
					$redirect_to = Noo_Member::get_candidate_profile_url();
				elseif( $user_args['role'] == Noo_Member::EMPLOYER_ROLE )
					$redirect_to = Noo_Member::get_company_profile_url();

				$redirect_to = isset($_POST['redirect_to']) && !empty($_POST['redirect_to']) ? $_POST['redirect_to'] : $redirect_to;
				$filter_tag = 'noo_register_redirect' . ( !empty($user_args['role']) ? '_'.$user_args['role'] : '' );

				$user_args['redirect_to'] = apply_filters($filter_tag, $redirect_to);
			}

			return $user_args;
		}

		public function require_confirmation( $errors = null, $new_user = array() ) {
			if ( is_wp_error( $errors ) && $errors->get_error_code() ) {
				return $errors;
			}

			if( self::get_setting('email_confirmation', false) && !empty( $new_user['user_email'] ) ) {

				// No confirm for social login
				if( isset( $new_user['using'] ) && !empty( $new_user['using'] ) ) {
					return $errrors;
				}

				$token = $this->_get_token( $new_user );

				if ( is_multisite() )
					$blogname = $GLOBALS['current_site']->site_name;
				else
					$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

				// user email
				$subject = sprintf(__('[%1$s]Verify your email address','noo'),$blogname);
				$to = $new_user['user_email'];
				$verify_link = esc_url( add_query_arg( array( 'token' => $token ), Noo_Member::get_endpoint_url('verify-email') ) );

				$message = __('Dear,<br/>
Thank you for registering an account on %1$s. Please <a href="%2$s">click here</a> or or use the following copy paste link to confirm this email address.<br/>
%3$s
<br/><br/>
Best regards,<br/>
%4$s','noo');
				$result = noo_mail($to, $subject, sprintf($message, $blogname, $verify_link, $verify_link, $blogname),array(),'noo_register$_confirm_email');
				if( $result ) {
					noo_message_add( __( 'Please confirm your email to complete registration.', 'noo' ), 'error' );
					$errors->add( 'email_confirmation', __( 'Please confirm your email to complete registration.', 'noo' ) );
				}
			}

			return $errors;
		}

		public function bypass_verifying_error( $errors = array(), $new_user = array() ) {
			if( isset( $new_user['email-verifying'] ) && !empty( $new_user['email-verifying'] ) ) {

				// Bypass error code form WP-SpamShield plugin
				if ( defined( 'WPSS_VERSION' ) && is_wp_error( $errors ) && $errors->get_error_code() ) {
					$error_codes = $errors->get_error_codes();
					$bypass_error_codes = array(
							'empty_first_name',
							'empty_last_name',
							'empty_disp_name',
							'jsck_error'
						);
					$bypass_error_codes = apply_filters( 'jm_spamshield_bypass_codes', $bypass_error_codes );
					foreach ( $bypass_error_codes as $code ) {
						if( in_array( $code, $error_codes ) ) {
							$errors->remove( $code );
						}
					}
				}
			}
			
			return $errors;
		}

		public function member_page_endpoint( $endpoints = array() ) {
			$endpoints = array_merge( $endpoints, array(
				'verify-email' => 'verify-email'
			) );

			return $endpoints;
		}

		public function verify_email_shortcode( $html = '', $query_vars = array() ) {
			if( isset( $query_vars['verify-email']) ){
				if( isset( $_GET['token'] ) ) {
					$user_args = $this->_check_token( $_GET['token'] );
					$user_args['email-verifying'] = true;
				}
				if( !empty( $user_args ) ) {
					remove_filter( 'noo_registration_errors', array( $this, 'require_confirmation' ), 10, 2 );
					$errors = Noo_Form_Handler::_register_new_user( $user_args );
					add_filter( 'noo_registration_errors', array( $this, 'require_confirmation' ), 10, 2 );

					if ( is_wp_error( $errors ) && $errors->get_error_code() ) {
						noo_message_add( $errors->get_error_message(), 'error' );
						wp_safe_redirect(Noo_Member::get_member_page_url());
						exit();
					} else {
						$redirect_to = isset($user_args['redirect_to']) && !empty($user_args['redirect_to']) ? $user_args['redirect_to'] : $redirect_to;
						$filter_tag = 'noo_register_redirect' . ( !empty($user_args['role']) ? '_'.$user_args['role'] : '' );
						
						noo_message_add( __('Your email is verified. Registration is completed.', 'noo') );
						wp_safe_redirect( esc_url( apply_filters($filter_tag, $redirect_to) ) );
						exit();
					}
				} else {
					noo_message_add( __('There is a problem of verifying your email, please try again.', 'noo'), 'error' );
					wp_safe_redirect(Noo_Member::get_member_page_url());
					exit();
				}
			}

			return $html;
		}

		private function _verify_email() {
			if( isset( $_GET['token'] ) ) {
				$data = $this->_check_token( $_GET['token'] );

				return $data;
			}
		}

		private function _get_token( $userData = array() ) {
			$token = sha1(uniqid());

			$oldData = get_option(self::PREFIX .'data');
			$oldData = !empty( $oldData ) ? $oldData : array();
			$data = array();
			$data[$token] = $userData;
			update_option(self::PREFIX .'data', array_merge($oldData, $data));

			return $token;
		}

		private function _check_token($token = '') {
			if( empty( $token ) ) return false;

			$data = get_option(self::PREFIX .'data');
			$userData = false;;

			if (isset($data[$token])) {
				$userData = $data[$token];
				unset($data[$token]);
				update_option(self::PREFIX .'data', $data);
			}

			return $userData;
		}

		public static function get_setting($id = null ,$default = null){
			$noo_member_setting = get_option('noo_member');
			if(isset($noo_member_setting[$id]))
				return $noo_member_setting[$id];
			return $default;
		}

		public function setting_fields() {
			?>
			<tr>
				<th>
					<?php _e('Require Email Confirmation','noo')?>
				</th>
				<td>
					<input id="email_confirmation" type="checkbox" name="noo_member[email_confirmation]" value="1" <?php checked( self::get_setting('email_confirmation', false) );?> />
					<small><?php echo sprintf( __('Newly registered users will be required to confirm Email before doing any action. Please note that this will delay the register function.', 'noo' ), 'Google' ); ?></small>
				</td>
			</tr>

			<?php
		}
	}

	new JM_EmailConfirmation();
endif;