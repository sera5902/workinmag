<?php

if( !function_exists( 'jm_get_job_custom_fields' ) ) :
	function jm_get_job_custom_fields( $all = true ) {
		$custom_fields = noo_get_custom_fields( 'noo_job_custom_field', 'noo_job_field_');

		if( $all ) {
			$default_fields = jm_get_job_default_fields();
			// $custom_fields = array_merge( array_diff_key($default_fields, $custom_fields), $custom_fields );
			$custom_fields = noo_merge_custom_fields( $default_fields, $custom_fields );
		}

		return apply_filters('jm_job_custom_fields' . ( $all ? '_all' : '' ), $custom_fields );
	}
endif;

if( !function_exists( 'jm_get_job_search_custom_fields' ) ) :
	function jm_get_job_search_custom_fields() {
		$custom_fields = jm_get_job_custom_fields();
		$date_field = array(
			'name' => 'date',
			'type' => 'datepicker',
			'label' => __('Publishing Date', 'noo'),
			'is_disabled' => 'no',
			'is_default' => true,
		);
		$custom_fields[] = $date_field;

		return apply_filters( 'jm_job_search_custom_fields', $custom_fields );
	}
endif;

if( !function_exists( 'jm_job_custom_fields_prefix' ) ) :
	function jm_job_custom_fields_prefix() {
		return apply_filters( 'jm_job_custom_fields_prefix', '_noo_job_field_' );
	}
endif;

if ( ! function_exists( 'jm_get_job_field' ) ) :
	function jm_get_job_field( $field_name = '' ) {
		
		$custom_fields = jm_get_job_custom_fields();
		if( isset( $custom_fields[$field_name] ) ) {
			return $custom_fields[$field_name];
		}

		$field_name = jm_job_custom_fields_name($field_name);
		if( isset( $custom_fields[$field_name] ) ) {
			return $custom_fields[$field_name];
		}


		return array();
	}
endif;

if( !function_exists( 'jm_job_custom_fields_name' ) ) :
	function jm_job_custom_fields_name( $field_name = '' ) {
		if( empty( $field_name ) ) return '';

		return apply_filters( 'jm_job_custom_fields_name', jm_job_custom_fields_prefix() . sanitize_title( $field_name ) );
	}
endif;

if( !function_exists( 'jm_get_job_custom_fields_option' ) ) :
	function jm_get_job_custom_fields_option($key = '', $default = null){
		$custom_fields = jm_get_setting('noo_job_custom_field', array());
		
		if( !$custom_fields || !is_array($custom_fields) ) {
			return $custom_fields = array();
		}

		if( isset($custom_fields['__options__']) && isset($custom_fields['__options__'][$key]) ) {

			return $custom_fields['__options__'][$key];
		}
	
		return $default;
	}
endif;

if( !function_exists( 'jm_job_custom_fields_menu' ) ) :
	function jm_job_custom_fields_menu() {
		add_submenu_page(
			'edit.php?post_type=noo_job',
			__( 'Custom Fields', 'noo' ),
			__( 'Custom Fields', 'noo' ),
			'edit_theme_options', 'job_custom_field',
			'jm_job_custom_fields_setting' );
	}
	
	add_action( 'admin_menu', 'jm_job_custom_fields_menu' );
endif;

if( !function_exists( 'jm_job_custom_fields_setting' ) ) :
	function jm_job_custom_fields_setting(){
		?>
		<div class="wrap">
			<form action="options.php" method="post">
				<?php 
				noo_custom_fields_setting( 
					'noo_job_custom_field',
					'noo_job_field_',
					jm_get_job_custom_fields()
				);

				$field_display = jm_get_job_custom_fields_option('display_position', 'after');
				?>
				<table class="form-table" cellspacing="0">
					<tbody>
							<tr>
								<th>
									<?php _e('Show Custom Fields:','noo') ?>
								</th>
								<td>
									<select class="regular-text" name="noo_job_custom_field[__options__][display_position]">
										<option <?php selected( $field_display,'before')?> value="before"><?php _e('Before Description','noo')?></option>
										<option <?php selected( $field_display,'after')?>  value="after"><?php _e('After Description','noo')?></option>
									</select>
								</td>
							</tr>
					</tbody>
				</table>
				<?php submit_button(__('Save Changes','noo')); ?>
			</form>
		</div>
		<?php
	}
endif;

if( !function_exists( 'jm_job_render_form_field') ) :
	function jm_job_render_form_field( $field = array(), $job_id = 0 ) {
		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field; 
		if( !isset( $field['name'] ) || empty( $field['name'] ) ) return;

		$field_id = '';
		if( isset( $field['is_default'] ) ) {
			if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') ) {
				return;
			}

			$field_id = $field['name'];
		} else {
			$field_id = jm_job_custom_fields_name( $field['name'] );
		}

		$value = !empty( $job_id ) ? noo_get_post_meta( $job_id, $field_id, '' ) : '';
		$value = !is_array($value) ? trim($value) : $value;

		$params = apply_filters( 'jm_job_render_form_field_params', compact( 'field', 'field_id', 'value' ), $job_id );
		extract($params);

		?>
		<div class="form-group row col-md-12">
			<label for="<?php echo esc_attr($field_id)?>" class="col-sm-3 control-label"><?php echo(isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'])  ?></label>
			<div class="col-sm-9">
				<?php noo_render_field( $field, $field_id, $value ); ?>
		    </div>
		</div>
		<?php
	}
endif;

if( !function_exists( 'jm_job_render_search_field') ) :
	function jm_job_render_search_field( $field = array() ) {
		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field; 
		if( !isset( $field['name'] ) || empty( $field['name'] ) ) return;

		$field_id = '';
		if( isset( $field['is_default'] ) ) {
			$field_id = $field['name'];
		} else {
			$field_id = jm_job_custom_fields_name($field['name']);
		}

		$field['required'] = ''; // no need for required fields in search form

		$value = isset($_GET[$field_id]) ? $_GET[$field_id] : '';
		$value = !is_array($value) ? trim($value) : $value;

		$params = apply_filters( 'jm_job_render_search_field_params', compact( 'field', 'field_id', 'value' ) );
		extract($params);
		?>
		<div class="form-group">
			<label for="<?php echo 'search-' . esc_attr($field_id)?>" class="control-label"><?php echo(isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'])  ?></label>
			<div class="advance-search-form-control">
				<?php
					if ( $field['type'] == "text" ) {
						global $wpdb;
						$field['value'] = $wpdb->get_col(
							$wpdb->prepare('
								SELECT DISTINCT meta_value
								FROM %1$s
								LEFT JOIN %2$s ON %1$s.post_id = %2$s.ID
								WHERE meta_key = \'%3$s\' AND post_type = \'%4$s\' AND post_status = \'%5$s\'
								', $wpdb->postmeta, $wpdb->posts, $field_id, 'noo_job', 'publish'));
						$field['type'] = 'select';
						$field['no_translate'] = true;
					}
					noo_render_field( $field, $field_id, $value, 'search' );
				?>
		    </div>
		</div>
		<?php
	}
endif;

if( !function_exists( 'jm_job_advanced_search_field' ) ) :
	function jm_job_advanced_search_field( $field_val = '' ) {
		if(empty($field_val) || $field_val == 'no' )
			return '';

		$field_arr = explode('|', $field_val);
		$field_id = isset( $field_arr[0] ) ? $field_arr[0] : '';

		if( empty( $field_id ) ) return '';

		$fields = jm_get_job_search_custom_fields();

		$field_prefix = jm_job_custom_fields_prefix();
		$field_id = str_replace($field_prefix, '', $field_id);

		foreach ($fields as $field) {
			if ( sanitize_title( $field['name'] ) == str_replace($field_prefix, '', $field_id) ) {
				jm_job_render_search_field( $field );
				break;
			}
		}
		return '';
	}
endif;

if( !function_exists( 'jm_job_save_custom_fields') ) :
	function jm_job_save_custom_fields( $post_id = 0, $args = array() ) {
		if( empty( $post_id ) ) return;

		// Update custom fields
		$fields = jm_get_job_custom_fields();
		if(!empty($fields)) {
			foreach ($fields as $field) {
				if( !isset( $field['name'] ) || empty( $field['name'] )) {
					continue;
				}

				$id = jm_job_custom_fields_name($field['name']);
				if( isset( $field['is_default'] ) ) {
					if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') ) {
						continue;
					}
					if( isset( $field['is_tax'] ) && $field['is_tax'] ) {
						continue;
					}

					$id = $field['name'];
				}

				if( isset( $args[$id] ) ) {
					noo_save_field( $post_id, $id, $args[$id], $field );
				}
			}
		}
	}
endif;

if( !function_exists( 'jm_job_display_custom_fields') ) :
	function jm_job_display_custom_fields() {
		$fields = jm_get_job_custom_fields();
		if(!empty($fields)) : ?>
			<div class="job-custom-fields">
				<?php 
				foreach ((array) $fields as $field):
					if( !isset( $field['name'] ) || empty( $field['name'] )) continue;
					$field['type'] = isset( $field['type'] ) ? $field['type'] : 'text';
					$id = jm_job_custom_fields_name($field['name']);
					if( isset( $field['is_default'] ) ) {
						if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') )
							continue;
						if( isset( $field['is_tax'] ) )
							continue;
						if( $field['name'] == '_closing' ) // reserve the _closing field
							continue;
						$id = $field['name'];
					}
					$value = noo_get_post_meta(get_the_ID(), $id, '');
					noo_display_field( $field, $id, $value );
				endforeach; ?>
			</div>
		<?php endif;
	}

	$field_pos = jm_get_job_custom_fields_option('display_position', 'after');
	if( $field_pos == 'before' ) {
		add_action( 'jm_job_detail_content_before', 'jm_job_display_custom_fields' );
	} else {
		add_action( 'jm_job_detail_content_after', 'jm_job_display_custom_fields' );
	}
endif;