<?php
/**
 * Export WooCommerce - Customers Section Settings
 *
 * @version 1.2.0
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_Export_Settings_Customers' ) ) :

class Alg_WC_Export_Settings_Customers extends Alg_WC_Export_Settings_Section {

	/**
	 * Constructor.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function __construct() {
		$this->id   = 'customers';
		$this->desc = __( 'Customers', 'export-woocommerce' );
		parent::__construct();
	}

	/**
	 * add_settings.
	 *
	 * @version 1.2.0
	 * @since   1.0.0
	 * @todo    (maybe) add "Additional Export Fields"
	 */
	function add_settings( $settings ) {
		$settings = array(
			array(
				'title'     => __( 'Export Customers Options', 'export-woocommerce' ),
				'type'      => 'title',
				'id'        => 'alg_wc_export_customers_options',
				'desc'      => '<p>' . '<em>' . alg_get_tool_description( $this->id ) . '</em>' . '</p>' . '<p>' . alg_get_tool_button_html( $this->id ) . '</p>',
			),
			array(
				'title'     => __( 'Export Customers Fields', 'export-woocommerce' ),
				'desc_tip'  => __( 'Hold "Control" key to select multiple fields. Hold "Control + A" to select all fields.', 'export-woocommerce' ),
				'id'        => 'alg_export_customers_fields',
				'default'   => alg_wc_export()->fields_helper->get_customer_export_default_fields_ids(),
				'type'      => 'multiselect',
				'options'   => alg_wc_export()->fields_helper->get_customer_export_fields(),
				'css'       => 'height:300px;min-width:300px;',
			),
			array(
				'type'      => 'sectionend',
				'id'        => 'alg_wc_export_customers_options',
			),
		);
		return $settings;
	}

}

endif;

return new Alg_WC_Export_Settings_Customers();
