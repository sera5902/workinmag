<?php
/**
 * WooCommerce Exporter Customers
 *
 * The WooCommerce Exporter Customers class.
 *
 * @version 1.1.0
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Alg_Exporter_Customers' ) ) :

class Alg_Exporter_Customers {

	/**
	 * Constructor.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function __construct() {
		return true;
	}

	/**
	 * export_customers.
	 *
	 * @version 1.1.0
	 * @since   1.0.0
	 */
	function export_customers( $fields_helper ) {

		// Standard Fields
		$all_fields = $fields_helper->get_customer_export_fields();
		$fields_ids = get_option( 'alg_export_customers_fields', $fields_helper->get_customer_export_default_fields_ids() );
		$titles = array();
		foreach( $fields_ids as $field_id ) {
			$titles[] = $all_fields[ $field_id ];
		}

		// Get the Data
		$data = array();
		$data[] = $titles;
		$args = array( 'role' => 'customer' );
		$args = alg_maybe_add_date_query( $args );
		$customers = get_users( $args );
		foreach ( $customers as $customer ) {
			$row = array();
			foreach( $fields_ids as $field_id ) {
				switch ( $field_id ) {
					case 'customer-id':
						$row[] = $customer->ID;
						break;
					case 'customer-email':
						$row[] = $customer->user_email;
						break;
					case 'customer-login':
						$row[] = $customer->user_login;
						break;
					case 'customer-nicename':
						$row[] = $customer->user_nicename;
						break;
					case 'customer-url':
						$row[] = $customer->user_url;
						break;
					case 'customer-registered':
						$row[] = $customer->user_registered;
						break;
					case 'customer-display-name':
						$row[] = $customer->display_name;
						break;
					case 'customer-first-name':
						$row[] = $customer->first_name;
						break;
					case 'customer-last-name':
						$row[] = $customer->last_name;
						break;
					case 'customer-debug':
						$row[] = '<pre>' . print_r( $customer, true ) . '</pre>';
						break;
					case 'nickname':
					case 'first-name':
					case 'last-name':
					case 'description':
					case 'billing-first-name':
					case 'billing-last-name':
					case 'billing-company':
					case 'billing-address-1':
					case 'billing-address-2':
					case 'billing-city':
					case 'billing-postcode':
					case 'billing-country':
					case 'billing-state':
					case 'billing-phone':
					case 'billing-email':
					case 'shipping-first-name':
					case 'shipping-last-name':
					case 'shipping-company':
					case 'shipping-address-1':
					case 'shipping-address-2':
					case 'shipping-city':
					case 'shipping-postcode':
					case 'shipping-country':
					case 'shipping-state':
//					case 'last-update':
						$row[] = get_user_meta( $customer->ID, str_replace( '-', '_', $field_id ), true );
						break;
				}
			}
			$data[] = $row;
		}
		return $data;
	}

	/**
	 * export_customers_from_orders.
	 *
	 * @version 1.1.0
	 * @since   1.0.0
	 * @todo    (maybe) add more order fields (shipping)
	 */
	function export_customers_from_orders( $fields_helper ) {

		$is_wc_version_below_3 = version_compare( get_option( 'woocommerce_version', null ), '3.0.0', '<' );

		// Standard Fields
		$all_fields = $fields_helper->get_customer_from_order_export_fields();
		$fields_ids = get_option( 'alg_export_customers_from_orders_fields', $fields_helper->get_customer_from_order_export_default_fields_ids() );
		$titles = array();
		foreach( $fields_ids as $field_id ) {
			$titles[] = $all_fields[ $field_id ];
		}

		// Get the Data
		$data = array();
		$data[] = $titles;
		$total_customers = 0;
		$orders = array();
		$offset = 0;
		$block_size = 1024;
		while( true ) {
			$args_orders = array(
				'post_type'      => 'shop_order',
				'post_status'    => 'any',
				'posts_per_page' => $block_size,
				'orderby'        => 'date',
				'order'          => 'DESC',
				'offset'         => $offset,
				'fields'         => 'ids',
			);
			$args_orders = alg_maybe_add_date_query( $args_orders );
			$loop_orders = new WP_Query( $args_orders );
			if ( ! $loop_orders->have_posts() ) {
				break;
			}
			foreach ( $loop_orders->posts as $order_id ) {
				$order = wc_get_order( $order_id );
				$_billing_email = $is_wc_version_below_3 ? $order->billing_email : $order->get_billing_email();
				if ( isset( $_billing_email ) && '' != $_billing_email && ! in_array( $_billing_email, $orders ) ) {
					$total_customers++;
					$row = array();
					foreach( $fields_ids as $field_id ) {
						switch ( $field_id ) {
							case 'customer-nr':
								$row[] = $total_customers;
								break;
							case 'customer-last-order-date':
								$row[] = get_the_date( get_option( 'date_format' ), $order_id );
								break;
							default:
								$_field_id = str_replace( array( 'customer-', '-' ), array( '', '_' ), $field_id );
								$row[] = alg_get_order_field( $order, $_field_id, $is_wc_version_below_3 );
								break;
						}
					}
					$data[] = $row;
					$orders[] = $_billing_email;
				}
			}
			$offset += $block_size;
		}
		return $data;
	}

}

endif;

return new Alg_Exporter_Customers();
