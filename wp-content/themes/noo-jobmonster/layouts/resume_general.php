<?php 
$resume_id = isset($_GET['resume_id']) ?absint($_GET['resume_id']) : 0;
$resume = $resume_id ? get_post($resume_id) : '';
$fields = jm_get_resume_custom_fields();
$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
?>
<?php do_action('noo_post_resume_general_before'); ?>
<div class="resume-form">
	<div class="resume-form-general row">
		<div class="col-sm-7">
			<div class="form-group row">
				<label for="title" class="col-sm-5 control-label"><?php _e('Resume Title','noo')?></label>
				<div class="col-sm-7">
			    	<input type="text" value="<?php echo ($resume ? $resume->post_title : '')?>" class="form-control jform-validate" id="title"  name="title" autofocus required>
			    </div>
			</div>
			<?php 
			if( !empty( $fields ) ) {
				foreach ($fields as $field) {
					jm_resume_render_form_field( $field, $resume_id );
				}
			}
			?>
			<?php if( Noo_Resume::get_setting('enable_video', '') ) : ?>
				<div class="form-group row">
					<label for="url_video" class="col-sm-5 control-label"><?php _e( 'Video URL', 'noo' ); ?></label>
					<div class="col-sm-7">
				    	<input type="text" value="<?php echo noo_get_post_meta( $resume_id, '_noo_url_video' ) ?>" class="form-control" id="url_video"  name="url_video" placeholder="<?php _e( 'Youtube or Vimeo link', 'noo' ); ?>" >
				    </div>
				</div>
			<?php endif; ?>
		</div>
		<?php if( Noo_Resume::get_setting('enable_upload_resume', '1') ) : ?>
			<div class="col-sm-5">
				<label for="file_cv" class="control-label"><?php _e('Upload your Attachment','noo')?></label>
				<div class="form-control-flat">
					<div class="upload-to-cv clearfix">
				    	<?php noo_file_upload_form_field( 'file_cv', jm_get_allowed_attach_file_types(), noo_get_post_meta( $resume_id, '_noo_file_cv' ) ) ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="col-sm-5">
			<div class="form-group">
			    <label for="desc" class="control-label"><?php _e('Professional Summary','noo')?></label>
			    <textarea class="form-control form-control-editor ignore-valid" id="desc" name="desc" rows="14" placeholder="<?php echo esc_attr__('Describe your resume in a few paragraphs','noo')?>"><?php echo ($resume ? $resume->post_content : '')?></textarea>
			</div>
		</div>
	</div>
</div>
<?php do_action('noo_post_resume_general_after'); ?>