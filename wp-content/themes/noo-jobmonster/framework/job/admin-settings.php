<?php

if( !function_exists( 'jm_job_settings_tabs' ) ) :
	function jm_job_settings_tabs( $tabs = array() ) {
		return array_merge( array(
				'general'=>__('Jobs','noo'),
				'email'=>__('Emails','noo'),
			), $tabs
		);
	}
	
	add_filter('noo_job_settings_tabs_array', 'jm_job_settings_tabs' );
endif;

if( !function_exists( 'jm_job_settings_form' ) ) :
	function jm_job_settings_form(){
		if(isset($_GET['settings-updated']) && $_GET['settings-updated']) {
			flush_rewrite_rules();
			do_action( 'jm_job_setting_changed' );
		}
		$archive_slug = jm_get_job_setting( 'archive_slug');
		$job_approve = jm_get_job_setting( 'job_approve','');
		$cover_image = jm_get_job_setting( 'cover_image','');
		$default_job_content = jm_get_job_setting( 'default_job_content', '');
		$submit_agreement = jm_get_job_setting( 'submit_agreement', null);
		$job_posting_limit = jm_get_job_setting( 'job_posting_limit',5);
		$job_display_duration = jm_get_job_setting( 'job_display_duration',30);
		$job_feature_limit = jm_get_job_setting( 'job_feature_limit',1);
		$job_posting_reset = jm_get_job_setting( 'job_posting_reset',0);

		?>
		<?php settings_fields('noo_job_general'); ?>
		<h3><?php echo __('Job Display','noo')?></h3>
		<table class="form-table" cellspacing="0">
			<tbody>
				<tr>
					<th>
						<?php esc_html_e('Job Archive base (slug)','noo')?>
					</th>
					<td>
						<input type="text" name="noo_job_general[archive_slug]" value="<?php echo ($archive_slug ? sanitize_title( $archive_slug ) :'jobs') ?>">
					</td>
				</tr>
				<tr>
					<th>
						<?php esc_html_e('Job Display','noo')?>
					</th>
					<td>
						<p><?php 
						$customizer_job_link = esc_url( add_query_arg( array('autofocus%5Bpanel%5D' => 'noo_customizer_section_job'), admin_url( '/customize.php' ) ) );
						echo sprintf( __('Go to <a href="%s">Customizer</a> to change settings for Job(s) layout or displayed sections.','noo'), $customizer_job_link); ?></p>
					</td>
				</tr>
				<?php do_action( 'noo_setting_job_display_fields' ); ?>
			</tbody>
		</table>
		<br/><hr/><br/>
		<h3><?php echo __('Job Posting','noo')?></h3>
		<table class="form-table" cellspacing="0">
			<tbody>
				<tr>
					<th>
						<?php esc_html_e('Job Approval','noo')?>
					</th>
					<td>
						<input type="hidden" name="noo_job_general[job_approve]" value="">
						<input type="checkbox" <?php checked( $job_approve, 'yes' ); ?> name="noo_job_general[job_approve]" value="yes">
						<p><small><?php echo __('Each newly submitted job needs the manual approval of Admin.','noo') ?></small></p>
					</td>
				</tr>
				<tr>
					<th>
						<?php esc_html_e('Enable Cover Image','noo')?>
					</th>
					<td>
						<input type="hidden" name="noo_job_general[cover_image]" value="">
						<input type="checkbox" <?php checked( $cover_image, 'yes' ); ?> name="noo_job_general[cover_image]" value="yes">
						<p><small><?php echo __('Allow Employers to change cover image for each job.','noo') ?></small></p>
					</td>
				</tr>
				<tr>
					<th>
						<?php esc_html_e('Default Job Content','noo')?>
						<p><small><?php echo __('Default content that will auto populated when Employers post new Jobs.','noo') ?></small></p>
					</th>
					<td>
						<?php
						$default_text = __('<h3>Job Description</h3><p>What is the job about? Enter the overall description of your job.</p>', 'noo');
						$default_text .= __('<h3>Benefits</h3><ul><li>What can candidates get from the position?</li><li>What can candidates get from the position?</li><li>What can candidates get from the position?</li></ul>', 'noo');
						$default_text .= __('<h3>Job Requirements</h3><ol><li>Detailed requirement for the vacancy.?</li><li>Detailed requirement for the vacancy.?</li><li>Detailed requirement for the vacancy.?</li></ol>', 'noo');
				        $default_text .= __('<h3>How To Apply</h3><p>How candidate can apply for your job. You can leave your contact information to receive hard copy application or any detailed guide for application.</p>', 'noo');

				        $text = !empty( $default_job_content ) ? $default_job_content : $default_text;
						
						$editor_id = 'textblock' . uniqid();
				        // add_filter( 'wp_default_editor', create_function('', 'return "tinymce";') );
				        wp_editor( $text, $editor_id, array(
				                    'media_buttons' => false,
				                    'quicktags' => true,
				                    'textarea_rows' => 15,
				                    'textarea_cols' => 80,
				                    'textarea_name' => 'noo_job_general[default_job_content]',
				                    'wpautop' => false)); ?>						
					</td>
				</tr>
				<tr>
					<th>
						<?php esc_html_e('Job submission condition','noo')?>
						<p><small><?php echo __('The condition that Employer must agree to before submitting a new job. Leave it blank for no condition.','noo') ?></small></p>
					</th>
					<td>
						<?php
						$submit_agreement = !is_null( $submit_agreement ) ? $submit_agreement : sprintf(__('Job seekers can find your job and contact you via email or %s regarding your application options. Preview all information thoroughly before submitting your job for approval.','noo'), get_bloginfo('name') );
						?>	
						<textarea name="noo_job_general[submit_agreement]" rows="5" cols="80"><?php echo esc_html($submit_agreement); ?></textarea>					
					</td>
				</tr>
				<?php $job_posting_mode = jm_get_job_posting_mode(); ?>
				<?php $job_posting_mode_list = jm_job_posting_mode_list(); ?>
				<?php if( count($job_posting_mode_list) == 1 ) : ?>
					<tr><td><input name="noo_job_general[job_posting_mode]" type="hidden" value="<?php reset( $job_posting_mode_list ); echo key( $job_posting_mode_list ); ?>"></td></tr>
				<?php elseif( count($job_posting_mode_list) > 1 ) : ?>
					<tr>
						<th>
						<?php esc_html_e('Job Posting Mode','noo')?>
						<p><small><?php echo __('Choose how you want users to be able to post jobs on your site.','noo') ?></small></p>
					</th>
					<td>
							<fieldset>
								<?php foreach ($job_posting_mode_list as $mode) : ?>
									<label><input type="radio" <?php checked( $job_posting_mode, $mode['value'] ); ?> name="noo_job_general[job_posting_mode]" value="<?php echo $mode['value']; ?>"><?php echo $mode['label']; ?></label><br/>
								<?php endforeach; ?>
							</fieldset>
						</td>
					</tr>
				<?php endif; ?>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$('input[name="noo_job_general[job_posting_mode]"]').change(function(event) {
							var $input = $( this );
							$('.job_posting_mode').hide();
							var value = $input.prop( "value" );
							$('.job_posting_mode-' + value ).show();
						});

						$('input[name="noo_job_general[job_posting_mode]"]:checked').change();
					});
				</script>
				<tr class="job_posting_mode job_posting_mode-free">
					<th>
						<?php esc_html_e('Job Limit','noo')?>
					</th>
					<td>
						<input type="text" name="noo_job_general[job_posting_limit]" value="<?php echo absint($job_posting_limit) ?>">
						<p><small><?php echo __('If you don\'t use Woocommerce Job Package for manage job submission, you can use this setting for limiting the number of jobs per employer.','noo') ?></small></p>
					</td>
				</tr>
				<tr class="job_posting_mode job_posting_mode-free">
					<th>
						<?php esc_html_e('Job Duration (day)','noo')?>
					</th>
					<td>
						<input type="text" name="noo_job_general[job_display_duration]" value="<?php echo absint($job_display_duration) ?>">
						<p><small><?php echo __('If you don\'t use Woocommerce Job Package for manage job submission, you can use this setting for job duration.','noo') ?></small></p>
					</td>
				</tr>
				<tr class="job_posting_mode job_posting_mode-free">
					<th>
						<?php esc_html_e('Featured Job Limit','noo')?>
					</th>
					<td>
						<input type="text" name="noo_job_general[job_feature_limit]" value="<?php echo absint($job_feature_limit) ?>">
						<p><small><?php echo __('If you don\'t use Woocommerce Job Package for manage job submission, you can use this setting for limiting the number of featured jobs per employer.','noo') ?></small></p>
					</td>
				</tr>
				<tr class="job_posting_mode job_posting_mode-free">
					<th>
						<?php esc_html_e('Reset counter every','noo')?>
					</th>
					<td>
						<input type="text" name="noo_job_general[job_posting_reset]" value="<?php echo absint($job_posting_reset) ?>">&nbsp;<?php echo __('Month', 'noo'); ?>
						<p><small><?php echo __('Reset the counter will allow Employers to re-post jobs after using up the limitation. Input zero for no reset.','noo') ?></small></p>
					</td>
				</tr>
				<?php do_action( 'noo_setting_job_submission_fields' ); ?>
			</tbody>
		</table>
		<?php 
	}
endif;

if( !function_exists( 'jm_email_settings_form' ) ) :
	function jm_email_settings_form(){
		$noo_notify_job_submitted_admin = jm_get_email_setting('noo_notify_job_submitted_admin');

		$noo_notify_register_employer = jm_get_email_setting('noo_notify_register_employer');
		$noo_notify_job_submitted_employer = jm_get_email_setting('noo_notify_job_submitted_employer');
		$noo_notify_job_review_approve_employer = jm_get_email_setting('noo_notify_job_review_approve_employer');
		$noo_notify_job_review_reject_employer = jm_get_email_setting('noo_notify_job_review_reject_employer');
		$noo_notify_job_apply_employer = jm_get_email_setting('noo_notify_job_apply_employer');

		$noo_notify_job_apply_attachment = jm_get_email_setting('noo_notify_job_apply_attachment');

		$noo_notify_register_candidate = jm_get_email_setting('noo_notify_register_candidate');
		$noo_notify_job_apply_candidate = jm_get_email_setting('noo_notify_job_apply_candidate');
		$noo_notify_job_apply_approve_candidate = jm_get_email_setting('noo_notify_job_apply_approve_candidate');
		$noo_notify_job_apply_reject_candidate = jm_get_email_setting('noo_notify_job_apply_reject_candidate');
		$noo_notify_resume_submitted_candidate = jm_get_email_setting('noo_notify_resume_submitted_candidate');

		$blogname = get_option('blogname');
		$from_name = jm_get_email_setting( 'from_name', $blogname );
		// $from_name = empty( $from_name ) ? $blogname : $from_name;
		$from_email = jm_get_email_setting( 'from_email', '' );

		?>
		<?php settings_fields('noo_email'); ?>
		<h3><?php echo __('Email Options','noo')?></h3>
		<table class="form-table" cellspacing="0">
			<tbody>
				<tr>
					<th>
						<?php _e('Admin Emails','noo')?>
					</th>
					<td>
						<p><input type="checkbox" name="noo_email[noo_notify_job_submitted_admin]" value="disable" <?php checked($noo_notify_job_submitted_admin,'disable')?>>
						<small><?php _e( 'Disable email job submitted email.', 'noo' ); ?></small></p>
					</td>
				</tr>
				<tr>
					<th>
						<?php _e('Employer Emails','noo')?>
					</th>
					<td>
						<p><input type="checkbox" name="noo_email[noo_notify_register_employer]" value="disable" <?php checked($noo_notify_register_employer,'disable')?>>
						<small><?php _e( 'Disable registration email.', 'noo' ); ?></small></p>
						<p><input type="checkbox" name="noo_email[noo_notify_job_submitted_employer]" value="disable" <?php checked($noo_notify_job_submitted_employer,'disable')?>>
						<small><?php _e( 'Disable job submitted email.', 'noo' ); ?></small></p>
						<?php if( jm_get_job_setting( 'job_approve') == 'yes' ) : ?>
							<p><input type="checkbox" name="noo_email[noo_notify_job_review_approve_employer]" value="disable" <?php checked($noo_notify_job_review_approve_employer,'disable')?>>
							<small><?php _e( 'Disable job review approved email.', 'noo' ); ?></small></p>
							<p><input type="checkbox" name="noo_email[noo_notify_job_review_reject_employer]" value="disable" <?php checked($noo_notify_job_review_reject_employer,'disable')?>>
							<small><?php _e( 'Disable job review rejected email.', 'noo' ); ?></small></p>
						<?php endif; ?>
						<p><input type="checkbox" class="employer_apply_notify" name="noo_email[noo_notify_job_apply_employer]" value="disable" <?php checked($noo_notify_job_apply_employer,'disable')?>>
						<small><?php _e( 'Disable job application notification email.', 'noo' ); ?></small></p>
					</td>
				</tr>
				<tr>
					<th>
						<?php _e('Candidate Emails','noo')?>
					</th>
					<td>
						<p><input type="checkbox" name="noo_email[noo_notify_register_candidate]" value="disable" <?php checked($noo_notify_register_candidate,'disable')?>>
						<small><?php _e( 'Disable registration email.', 'noo' ); ?></small></p>
						<p><input type="checkbox" name="noo_email[noo_notify_job_apply_candidate]" value="disable" <?php checked($noo_notify_job_apply_candidate,'disable')?>>
						<small><?php _e( 'Disable job application notification email.', 'noo' ); ?></small></p>
						<p><input type="checkbox" name="noo_email[noo_notify_job_apply_approve_candidate]" value="disable" <?php checked($noo_notify_job_apply_approve_candidate,'disable')?>>
						<small><?php _e( 'Disable job application approved email.', 'noo' ); ?></small></p>
						<p><input type="checkbox" name="noo_email[noo_notify_job_apply_reject_candidate]" value="disable" <?php checked($noo_notify_job_apply_reject_candidate,'disable')?>>
						<small><?php _e( 'Disable job application rejected email.', 'noo' ); ?></small></p>
						<p><input type="checkbox" name="noo_email[noo_notify_resume_submitted_candidate]" value="disable" <?php checked($noo_notify_resume_submitted_candidate,'disable')?>>
						<small><?php _e( 'Disable resume subitted email.', 'noo' ); ?></small></p>
					</td>
				</tr>
				<tr class="employer_apply_notify-child">
					<th>
						<?php _e('Email Attachment','noo')?>
					</th>
					<td>
						<p><input type="checkbox" name="noo_email[noo_notify_job_apply_attachment]" value="enable" <?php checked($noo_notify_job_apply_attachment,'enable')?>>
						<small><?php _e( 'Include application attachment in Employer email.', 'noo' ); ?></small></p>
					</td>
				</tr>
				<script>
					jQuery(document).ready(function($) {
						if( $("input.employer_apply_notify").is(":checked") ) {
							$('.employer_apply_notify-child').hide();
						} else {
							$('.employer_apply_notify-child').show();
						}

						$("input.employer_apply_notify").change( function() {
							if( $(this).is(":checked") ) {
								$('.employer_apply_notify-child').hide();
							} else {
								$('.employer_apply_notify-child').show();
							}
						} );
					});
				</script>
			</tbody>
		</table>
		<hr/>
		<table class="form-table" cellspacing="0">
			<tbody>
				<tr>
					<th>
						<?php _e('From Email','noo')?>
					</th>
					<td>
						<input type="text" name="noo_email[from_email]" placeholder="<?php echo noo_mail_do_not_reply(); ?>" size="40" value="<?php echo esc_attr($from_email); ?>">
						<p><small><?php _e( 'The email address that emails on your site should be sent from. You should leave it blank if you used a 3rd plugin for sending email.', 'noo' ); ?></small></p>
					</td>
				</tr>
				<tr>
					<th>
						<?php _e('From Name','noo')?>
					</th>
					<td>
						<input type="text" name="noo_email[from_name]" placeholder="<?php echo get_option('blogname'); ?>" size="40" value="<?php echo esc_attr($from_name); ?>">
						<p><small><?php _e( 'The name that emails on your site should be sent from. You should leave it blank if you used a 3rd plugin for sending email.', 'noo' ); ?></small></p>
					</td>
				</tr>
				<?php do_action( 'noo_setting_email_fields' ); ?>
			</tbody>
		</table>
		<?php 
	}
endif;
