��         �  %  �1      0B     1B     9B     ;B  |   HB  �   �B  &   LC     sC     �C      �C  
   �C  	   �C     �C     �C     �C     �C     D  	   D     D  #   )D  	   MD  S   WD     �D  6   �D  
   �D  
   �D     E     E     +E     ;E  
   NE     YE     qE     �E     �E  
   �E     �E     �E     �E  �   �E     �F     �F     �F  	   �F     �F     �F     �F     �F     �F     �F     �F     �F     �F     �F  
   G     G     G     )G     1G     EG     QG  	   fG     pG  
   xG     �G     �G     �G     �G     �G     �G     �G     �G  	   �G     �G     H     H     H     ,H     DH     QH     `H     lH     �H     �H     �H     �H     �H     �H     �H     I     I     !I     5I     LI     UI     mI      rI     �I     �I     �I     �I     �I     �I  	   �I     �I     �I     �I     �I     J     J     &J     6J     =J     MJ     dJ     {J  *   �J     �J  %   �J  	   �J     �J     K     %K     4K     SK  %   gK     �K     �K     �K     �K     �K     �K      �K  ,   �K  (   *L     SL  %   dL     �L  
   �L     �L     �L     �L     �L     �L     �L     M  	   M     &M     8M     @M     TM     gM     tM     �M     �M     �M     �M     �M  �  �M  �   �O  @   IP  9   �P     �P     �P     �P     Q     ,Q     HQ     cQ     ~Q     �Q     �Q  #   �Q  >   �Q     	R  
   R      R     2R     GR     aR     iR     yR     �R     �R  B   �R     �R     �R     S  �   S  �   �S     �T  �   �T     `U     gU  
   zU     �U     �U     �U     �U  !   �U  %   �U  (   V  	   ;V     EV     MV  "   ^V  %   �V  .   �V  $   �V  }   �V     yW     �W     �W     �W     �W     �W  	   �W     �W     �W     �W     X     X     X  	   )X     3X     9X     IX     YX  Z   oX  T   �X     Y     &Y     /Y     DY     WY  %   jY  6   �Y  *   �Y     �Y      �Y  #   Z     >Z     [Z     nZ     �Z     �Z     �Z     �Z     �Z     [     [  !   #[      E[  !   f[  #   �[  )   �[  /   �[     \     \     !\  2   -\     `\     l\     \     �\     �\     �\     �\     �\  	   �\     �\  
   �\     �\     �\     	]  	   ]  	   ]     )]     <]     P]     `]     c]     p]  	   ~]  	   �]  	   �]  	   �]  	   �]  	   �]  �   �]  �   �^    �_  +  �`  �   �a  �   �b  �   �c  �   kd     ;e     @e     Me     be  �   se  !   af     �f     �f  F   �f  [   �f  <   Mg     �g     �g     �g     �g     �g     �g     �g     �g     �g  8    h     9h     Eh     ah     hh  	   lh  	   vh     �h  
   �h     �h     �h     �h     �h     �h     �h     i     i  	   &i     0i     9i     Ii     Wi     hi     ui     �i  	   �i     �i     �i     �i     �i     �i     �i     j     *j     Cj     Sj  	   cj     mj     �j  	   �j  �   �j  !   Ok     qk     �k     �k     �k     �k     �k     �k     �k  	   �k     �k     �k     l     l  I   'l      ql     �l     �l     �l     �l     �l     �l     �l  	   �l     �l     m  a   m     mm     sm     �m      �m     �m     �m     �m     �m     
n     n     <n  |   Jn     �n     �n     �n     �n     o     o     &o     9o     Ho     Po     ^o     go     yo  	   �o     �o  
   �o     �o     �o     �o     �o     �o     �o     �o     �o     �o  �   p  	   �p     �p     �p     �p     �p     �p     q  "   q     Bq     Pq     gq     {q     �q     �q      �q     �q     �q     �q  *   r     1r     Er     Sr  
   _r     jr     or     ur     �r     �r     �r     �r     �r  1   �r  .   s  #   Ns  "   rs  *   �s  %   �s  -   �s  -   t     Bt     \t     et     ht     ut     �t     �t     �t     �t     �t     �t  .   �t  +   �t     u     <u     Tu     Yu     au     xu     �u     �u  "   �u     �u     �u  &   �u  8   v  3   Ov     �v     �v  #   �v  6   �v     w     (w  9   ?w  &   yw  $   �w     �w     �w  (    x     )x  %   Hx     nx     �x     �x  
   �x     �x     �x     �x     �x     �x     �x     �x     y     y  !   y     Ay     Oy     ly     ty     �y     �y     �y  	   �y     �y     �y     �y     �y     �y     �y     �y     	z     z     *z     7z     Cz     Jz     Zz     `z     {z     �z     �z     �z     �z     �z     �z     �z     �z     {     {     .{     A{      N{     o{     w{     �{     �{     �{     �{     �{     �{     �{     �{     �{  
   �{      |     |     &|     :|     H|     a|     u|  $   �|     �|     �|     �|     �|     �|  	   �|     �|     	}     }     $}     8}     I}     d}     �}     �}     �}     �}     �}     �}  G   �}     4~     =~     C~     S~     e~     v~     �~     �~     �~     �~     �~     �~     �~     �~     �~                    ,     ?  
   T     _  
   c     n  G   t     �  %   �  "   �      %�     F�     d�  I   s�     ��     Ā     ˀ     ڀ     �      �     �     #�     =�     J�      V�     w�      ��     ��  :   Ё  =   �  (   I�  W   r�  Z   ʂ  @   %�  4   f�      ��  =   ��     ��  *   �  ?   =�     }�  G   ��  &   ڄ  [   �  8   ]�     ��  4   ��     х     �     ��     �     �     %�     6�  
   ;�  	   F�  =   P�     ��     ��     ��  	   Ɔ     І     ކ     �     �     �  	    �     *�  1   7�     i�     ��     ��     ��     ��  	   Ç     ͇  	   ҇     ܇     �     �     �  "   �  	   6�  	   @�     J�     ]�  )   f�     ��  .   ��  $   ӈ     ��      �     �     �     �     9�  7   I�     ��     ��     ��     ��  !   É     �  L   �     P�     l�  4   ��  #   ��  0   �  [   �  ;   n�  *   ��  B   Ջ  %   �  +   >�  L   j�  E   ��     ��  &   �  2   ?�  "   r�     ��  +   ��  [   ۍ     7�     S�     n�     ��     ��     ��  *   Ҏ     ��     �  !   0�     R�     i�  4   ��     ��  2   я     �     �  .   .�  
   ]�  J   h�  2   ��     �  4   ��     +�  2   :�  	   m�  $   w�  9   ��  1   ֑     �     �     4�     6�     S�     o�      ��  ,   ��  '   ܒ  4   �     9�     Y�     m�     ~�     ��  
   ��      ��     Ó     �     �     �     �     �     �      �     7�  	   9�  n  C�     ��     ��     ��  �   ʗ  �   M�  #   �     	�     �  -   +�     Y�  
   f�     q�     }�     ��     ��     ��  
   ؙ     �  "   �     �  \   "�     �  4   ��  
   Ś  
   К     ۚ     ��     �      �     ;�     J�     f�     ��     ��     ��     қ     �     ��  �   ��     ��  
   ��     ��     Ҝ     ޜ     �     ��     ��     �     �     �     �     .�     A�     U�     g�     v�     ��     ��     ��  #   ͝     �  
   �     �     �     %�     2�     G�     ]�     p�     ��     ��     ��     ��     ڞ     �      �     �     1�  	   :�  	   D�  "   N�     q�     ��     ��     ��  $   ǟ     �     �     #�     8�     @�     R�     p�     y�     ��  *   ��     Ơ     ̠     �     �     �     �     �     "�     *�     /�     <�     J�     h�  
   }�     ��     ��  #   ��  "   ˡ  '   �  I   �     `�  3   i�  	   ��  "   ��     ʢ     �  5   ��     3�  7   I�     ��  
   ��     ��     ��     £     ٣  *   �  >   �  6   R�     ��  .   ��     Τ     �     ��     �  *   �     =�     D�     T�      o�     ��     ��     ��     ĥ     �     ��     �     (�     =�     Z�  !   n�     ��  �  ��  �   ��  ;   ��  A   ��      �  +   �  .   9�  ,   h�  ,   ��  *   ª  ,   ��     �  	   ,�     6�  ,   D�  V   q�     ȫ     ګ     �     ��     �     8�     F�     U�     c�  !   v�  f   ��     ��     �     �  �   2�  �   ��     ��  �   ˮ     ��     ��     ��     ʯ     �     ��     �      #�  +   D�     p�  
   ��  
   ��     ��  -   ��  ;   ް  5   �  #   P�  �   t�  	    �     
�     �     "�  !   1�     S�     c�     o�     }�     ��     ��     Ȳ     ֲ  
   �     ��     ��      �  (   6�  �   _�  ~   �     o�  	   ��     ��     ��     ��  0   ִ  <   �  /   D�     t�  +   |�  D   ��  ?   ��     -�     F�  %   c�     ��  %   ��     ϶     �     �     �      4�  !   U�  !   w�      ��  ,   ��  *   �     �     +�     ;�  >   K�     ��     ��     ��     ��     ͸  	   ڸ     �     ��     
�     �     �     &�     ,�  	   I�  
   S�     ^�     n�     ��     ��     ��     ��     ��     й     ٹ     �     �     ��     ��    �  �   �     ��  V  �  �   q�  �   p�  �   p�  �   ]�     =�     C�     T�     i�  �   ��  '   x�  $   ��     ��  I   ��  �   �  B   ��     ��     ��     ��     ��     �     �     2�     @�     [�  K   k�     ��  (   ��     ��     ��     �     �  (   $�     M�     U�  (   k�     ��     ��     ��     ��  	   ��     ��     �      �     2�     Q�     b�     u�     ��     ��     ��     ��     ��     ��  ,   �     /�     N�     k�      ��     ��     ��     ��     ��     �     �  �   '�  :   '�     b�     ��     ��     ��     ��     ��     ��     ��     ��  
   �     �     )�     5�  L   >�  )   ��     ��     ��     ��     ��     ��     ��     �     "�     .�  
   =�  l   H�     ��     ��     ��  3   ��     )�     7�     S�     p�     ��     ��     ��  �   ��     ��     ��     ��     ��  (   ��     ��  $   ��     �     ,�     4�     =�     F�     N�     V�     ^�  	   s�     }�     ��     ��     ��     ��     ��     ��     ��  #   ��  �   	�     ��     ��     ��     �     �  (   1�  (   Z�  +   ��     ��  '   ��     ��  $   �  !   7�     Y�  Z   h�     ��     ��  +   ��  <   #�  +   `�     ��     ��     ��     ��     ��     ��  0   ��     �     %�  &   C�     j�  C   x�  >   ��  (   ��  (   $�  8   M�  9   ��  6   ��  5   ��     -�     K�     T�     V�     e�  	   s�     }�     ��  	   ��     ��     ��  3   ��  D   ��  &   8�     _�     ~�  	   ��     ��     ��     ��     ��  6   ��  +   �     ;�  '   W�  8   �  E   ��  +   ��  ,   *�  C   W�  T   ��     ��     �  B   %�  @   h�  9   ��  *   ��  7   �  6   F�  *   }�  1   ��     ��     ��     �     �     �  	   -�     7�     H�  *   a�     ��     ��     ��     ��  $   ��     ��  "   ��     �     %�     5�     I�     \�  	   `�     j�     s�     �     ��     ��     ��     ��     ��     ��     ��     �     �     !�  	   7�     A�  3   ]�     ��     ��  	   ��     ��     ��     ��     �     %�     8�  #   O�     s�     ��  $   ��  
   ��     ��      ��     	�      �     (�     8�     J�  @   d�     ��     ��     ��     ��     ��     �     "�  "   5�     X�     q�  7   ��     ��     ��     ��     ��     �     �     �     2�     E�     ]�     y�     ��     ��     ��     ��     ��  +   �     3�     N�  X   a�     ��  	   ��     ��     ��     ��     �     )�     @�     U�     j�     |�     ��     ��     ��     ��     ��     ��  !   ��     �  !   2�  	   T�     ^�     c�     y�  I   ��  '   ��  6   ��  1   +�  ,   ]�  0   ��     ��  N   ��      �     '�     .�  #   @�     d�     q�     ��  )   ��     ��     ��     ��     �  4   !�  %   V�  8   |�  S   ��  2   	�  d   <�  v   ��  U   �  C   n�  '   ��  C   ��     �  3   6�  ?   j�     ��  f   ��  *   !�  �   L�  A   ��     �  @   �     X�     i�     ��     ��     ��     ��     ��     ��     ��  D   ��     4�     M�     d�     v�     ��     ��     ��     ��     ��     ��     ��  4   ��     *�     =�     S�  '   e�     ��     ��     ��     ��     ��     ��     ��     ��  7   �     <�     H�     Q�     f�  *   n�     ��  >   ��  /   ��  	   �     '�     /�     @�      H�     i�  :   }�     ��     ��  G   ��  &   �     =�  !   Y�  \   {�     ��     ��  <   �  -   N�  2   |�  n   ��  @   �  )   _�  Q   ��  (   ��  *   �  N   /�  K   ~�     ��  +   ��  N   �  :   b�  %   ��  3   ��  �   ��  ;   x�     ��     ��  !   ��     �     "�  0   ?�     p�  '   ��     ��     ��     ��  A   �  (   R�  ?   {�  #   ��     ��  8   ��     ,�  �   5�  A   ��  	   ��  4   �     9�  .   E�  	   t�  )   ~�  D   ��  >   ��     ,�  )   K�     u�  #   w�  %   ��  '   ��  #   ��  8   �  *   F�  =   q�  4   ��     ��     �     �     6�     =�  !   L�  $   n�     ��     ��     ��     ��     ��     ��     ��     ��     ��                 �     �  �   %        6           �  �      w   �      �   �  �    �  �        �               �       Z  �  ,      �            �       2  7      \      l  Y  �   8  9  �             �  /  �   �  �  {   �  )   1   �  i         h              �        �   �  	  �   �          �  �       �   �   �  *      u          w            �   �      .  �   �   �       �  �      y   �  H   j  �  �  �      T  �   Y  �   �      =   �  Q      �          �  �   v       �   ^      ]       r          q  �      �  �  �  �  h  �  3  A  �  e   "  5  B   "   �  �      B  O  �          �    �     4       �  �       g             �  �         H  �      >  �  �      P  �           [  �   :      �  �   �  r    ]  �   �  o         �  L  #  K          �  �          �   �  �      �  L    �               �   �  �               �     @      �               -     �       �      �   �  b      4    �  �  �           a  �  o  �   <   �  D                E  �  y    �           �            N             t    �  �      �       �  �    `  
               t      �  �  �  �  9           D   i       �  �  �         C          g  �   :  �   0  =  f    >  @                   *  Z  �  �       �   x   �  +  )  0   �  �          �  X   _   .  �  &  �         �  �   �  �  I        j   ]  �  4  �  }  !   �        �            �  �   M             �  D  �  s   c   �  �           �   �      �  %  �  k     �   �       
       &  f     �      $     /   |   �   �   �   R  �  <  �    e  �      �  �       )  A   k   I      �     :   �      �   �   J        �  �   �      �     6  �      b           �   W  |  R      7         �  $          �       z      �  n       �       �  m  �    "      �  �  �   
  T  2  �  �   �   �   (  �      �  �   �         �  	       �  �      �    �  [  �   �  O   �  �           f       �  A      �       n    �     p  1  �  +  a  �  y      �           �   !  �  �   �  �  ~    �       s  �  &   �  S  S        T   �      �   �  �  u    V  ;     �  ~   �       }      {      �      P       �  �   �      c     �  �      a   �  �      �   v      �  �  �   �  ?  �  t   \   �      C  W  8   '     n  F  �      �  �   �   Y     �  �  �   ^             3   |    >       �  �     �  �  z      �                 ^  B  M  �  ?  �  F  �    �          �           �  m  M   C  �      �  �   �       �          -      _  i      s    U        w  �   �      �   �            �         J  �  H  q   �  �       �               �  z           �      U   }   �           �          �          �   �  �      �       v  (   �    �  �         1    F           �  �  %   �          S   �           �  �   �         �  �  I   r       �          �   �  �  ,  `  c  5          g             �  R      �   8  �          `   �      @  P  �      �      �  �       �   d                �              �  N  o   �  5   �   �     �   �  �       p     �       p  <  d   �  e  �  �      b  /  �  0  �      �  _  �  j  �  �   �  �  �  �  �      G                 W   �     E            �           �  �  ;  �  �  �  �  O  '   �  G        Z     �  
  V  �  .         �  K   (  L       q  J          �  ?   �  �        �        ,  U  X        �         l       6  k  N      �  =  ~        [     �              X  -  �  �   9      d      �  x  �          �  �    �   �       �   �   �      �  �  u       �  +   �   �     �   Q       	  �  �         3          �   $   G  �    �          x  �  	  ;  �      2           \              �      �      �   l  �       !  Q  �  �   �  �  �   �     #      �  7  �          '  �  K  #   �  �   {      �  �  �  �  V   *     �       �  m   �  h                   �  �  E     for %s % % (1 to 100) %1$s has just posted a job:<br/><br/>
				<a href="%2$s">View Job Detail</a>.
				<br/><br/>
				Best regards,<br/>
				%3$s %1$s has just submitted a job for review.<br/><br/>
				<a href="%2$s">View Job Detail</a>.<br/><br/>

				Best regards,<br/>
				%3$s %1$s has responded to your application %d applications %d views %d+ New Jobs - Job Alert from %s %s License %s day(s) %s days ago %s featured job %s has posted %s jobs %s hours ago %s job posting %s job(s) %s minutes ago %s published. <a href="%s">View</a> %s saved. %s scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview</a> %s seconds ago %s submitted. <a target="_blank" href="%s">Preview</a> (Optional) (optional) ** Job Category: %s ** Job Type: %s ** Location: %s + Add New Location - Select - - Select a Candidate -  - Select a category - - Select a location - - Select an Employer -  -All jobs- -Bulk Actions- -Select- ... <h3>Job Requirements</h3><ol><li>Detailed requirement for the vacancy.?</li><li>Detailed requirement for the vacancy.?</li><li>Detailed requirement for the vacancy.?</li></ol> A A Job A Post A Product A Resume Action Action: Actions Activate Active Add Add Company Add Education Add Experience Add Images Add Job Add Job Application Add New Add New Application Add New Job Add New Job Location Add Skill Address Alert Name All All %s All Candidates All Categories All Companies All Employers All Locations All Messages%s All Types All done. Have fun!  All jobs Allowed Files Allowed file: %s Already have account ?  Announcement Announcement%s Application Application via "%s" on %s Applied Date Applied job Apply For Job Message Apply for job Apply for job via LinkedIn Apply for this job Apply via LinkedIn Apply with LinkedIn Approve Approve Application Approve/Reject Message Approved Approved %s application Apps Are you sure to delete this job? Author Available Jobs Back Behance Profile Best regards, Birthday Blog Page Bold Book Book Italic Bookmark Job Bookmark cleared. Bookmark removed. Bookmarked Jobs Browse CV | Attachment Can find this Package. Can not find this job. Can not find this resume. Can not get email from your social account Cancel Candiates can start conversation with Candidate Candidate Basic Info Candidate Social Profile Candidate name Candidate needs a valid Email. Candidate's Message Candidates who applied for their jobs Cart Subtotal Category Change File Change Image Change Password Change Profile Changes were saved successfully. Check your e-mail for the confirmation link. Choose Layout settings for your Job List Choose a Package Choose a Package That Fits Your Needs Choose a package Clear File Clear Gallery Clear Image Click here to cancel the reply Closing Closing Date Comment navigation Comments are closed. Companies Companies Listing Company Company Description Company Infomation Company Logo Company Name Company Profile Company Website Company updated Complete the setup now. Confirm new password Congratulation %1$s,<br/><br/>
You've successfully applied for %2$s.<br/>
<a href="%3$s">View Job Detail</a><br/>
You can manage and follow status of your applied jobs and applications in <a href="%4$s">My Applications</a>.
<br/><br/>
Note: Due to high application volume, employers may not be able to respond to all the application.
<br/><br/>
Good luck on your future career path!
<br/><br/>
Best regards,<br/>
%5$s Congratulation! 
We received your application for the job and found your skills and experience matched our requirement. We will contact you for detail of the second selection round soon.
Best regards. Congratulation! You've successfully created an account on [%1$s] Congratulation! Your resume passed our application round. Connect with us Connect with us on Facebook Connect with us on Google Plus Connect with us on Instagram Connect with us on LinkedIn Connect with us on Twitter Connect with us on Website Contact Email Continue Continue reading Could not add a new job application Couldn't register you... please contact the site administrator Cover Image Create New Create New Resume Create new Job alert Create user successfully. Current Current Company Current Job Current Password Custom Application URL Custom link to redirect job seekers to when applying for this job. Daily Date Activated Date Modified Dear %1$s,<br/>
Thank you for registering an account on %2$s as a candidate. You can start searching for your expected jobs or create your resume now.
<br/><br/>
Best regards,<br/>
%3$s Dear %1$s,<br/>
Thank you for registering an account on %2$s as an employer. You can start posting jobs or search for your potential candidates now.
<br/><br/>
Best regards,<br/>
%3$s Dear %s, Dear,<br/>
Thank you for registering an account on %1$s. Please <a href="%2$s">click here</a> or or use the following copy paste link to confirm this email address.<br/>
%3$s
<br/><br/>
Best regards,<br/>
%4$s Delete Delete Application Delete Job Delete Job Alert Delete Resume Deleted %s application Deleted %s job Describe your company and vacancy Describe your job in a few paragraphs Describe your resume in a few paragraphs Directory Disable Disable viewable Display the Categories for resume. Display the number of available jobs. Display the total number of available resumes. Do you want to continue this action? Don't have an account yet? <a href="%s" class="member-register-link" >Register Now <i class="fa fa-long-arrow-right"></i></a> Download Download My Attachment Edit Edit Company Edit Company Info Edit Gallery Edit HTML Edit Job Edit Job Alert Edit Job Application Edit Job alert Edit Profile Edit Resume Education Email Email Candidate Email Frequency Email already exists. Email to receive application notification. Leave it blank to use Employer's profile email. Email to receive application notification. Leave it blank to use your account email. Email* Employer Employer Information Employer's Message Employer's message Employers can start conversation with Employers who approved or rejected their applications. Employers who approved their applications. Enable Enter a short title for your job Enter a username or e-mail address. Enter keywords to match jobs Enter new location Enter the text you see Enter your company description Enter your company name Enter your company website Enter your email here... Error in deleting. Error publish. Error unpublish. Error when approving application. Error when deleting application. Error when rejecting application. Error when withdrawing application. Everyone ( all candidates and employers ) Everyone ( all candidates and other employers ) Expected Job Level Expired Expired Job Expired listings will be removed from public view. Extra Light Extra Light Italic Facebook Profile Facebook Share Facebook URL Featured Featured Job limit Featured Jobs Featured? Filter: First Name Five Forgot Password? Fortnightly Frequency Full Name General Infomation General Information General Options Go Google + URL Google+ Share Heading 1 Heading 2 Heading 3 Heading 4 Heading 5 Heading 6 Hi %1$s,
<br/><br/>
You've posted a new resume:<br/>
Title: %2$s<br/>
Location: %3$s<br/>
Category: %4$s<br/>
<br/><br/>
You can manage your resumes in <a href="%5$s">Manage Resume</a>.
<br/><br/>
Best regards,<br/>
%6$s Hi %1$s,<br/>
				<br/>
				%2$s've just applied for %3$s.<br/>
				<a href="%4$s">View Resume</a><br/>
				You can manage applications for your jobs in <a href="%5$s">Manage Application</a>.
				<br/><br/>
				Best regards,<br/>
				%6$s Hi %1$s,<br/>
%2$s has just responded to your application for job  <a href="%3$s">%4$s</a> with message: 
<br/>
<div style="font-style: italic;">
%5$s
</div>
<br/>
You can manage your applications in <a href="%6$s">Manage Application</a>.
<br/>
Best regards,<br/>
%7$s Hi %1$s,<br/><br/>
				You've submitted a new job:<br/>
				<a href="%2$s">View Job Detail</a>.
				<br/><br/>
				We will review your job and replied back to you soon.<br/>
				You can manage and follow sattus of your jobs in <a href="%3$s">Manage Jobs</a><br/><br/>
				Best regards,<br/>
				%4$s Hi %1$s,<br/><br/>
				You've successfully post a new job:<br/>
				<a href="%2$s">View Job Detail</a>.
				<br/><br/>
				You can manage your jobs in <a href="%3$s">Manage Jobs</a><br/><br/>
				Best regards,<br/>
				%4$s Hi %1$s,<br/><br/>
Your submitted job %2$s can not be published and has been deleted. You will have to submit another job.
<br/><br/>
You can manage your jobs in <a href="%3$s">Manage Jobs</a><br/><br/>
Best regards,<br/>
%4$s Hi %1$s,<br/><br/>
Your submitted job %2$s has been approved and published now on %3$s:<br/>
<a href="%4$s">View Job Detail</a>.
<br/><br/>
You can manage your jobs in <a href="%5$s">Manage Jobs</a><br/><br/>
Best regards,<br/>
%6$s Hi,
We received your application for the job and found your skills and experience does not match our requirement. Thank you for your interest in our vacancy and good luck in your future career.
Best regards. Hi,  Hide Details Highest Degree Level I agree with the I am very interested in the %s position at %s. I believe my skills and work experience make me an ideal candidate for this role. I look forward to speaking to you about this position soon. Thank you for your consideration.
Best regards 
 I'm a candidate looking for a job I'm an employer looking to hire ID: %d If this was a mistake, just ignore this email and nothing will happen. In this section you have settings for your Job List page, Archive page and Single Job page. In this section you have settings for your Resume List page. Inactive Indent Information Insert image Insert link Instagram Profile Instagram URL Introduce Yourself Invalid Email Invalid confirmation code, please enter your code again. Invalid job Invalid username or e-mail. Italic Job Job Added Job Alert Job Alert needs a name. Job Alerts Job Application Job Application Settings Job Applications Job Category Job Description Job Details Job Duration Job Duration (day) Job Limit Job List Job List Layout Job List Page Job List Sidebar Job Location Job Package Job Tag Job Title Job Type Job alert deleted Job alert saved Job application draft updated. Job application published. Job application saved. Job application submitted. Job application updated. Job applied for Job bookmarked. Job count Job displayed for %s days Job not found ! Job saved Job seekers can find your job and contact you via email or %s regarding your application options. Preview all information thoroughly before submitting your job for approval. Job set to featured successfully. Job submission condition Job successfully added Job updated Job's Expiration Date Jobs Keyword Keywords Language Last Name Latest Jobs Leave a reply to %s Leave your thought License License <a target="_blank" href="%s" style="">How to get License key?</a> License e-mail or domain address License key License key: Light Light Italic LinkedIn Profile LinkedIn Share Linkedin URL Load More Loading Maps Location Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Login Login as Employer Login or create an account Login successful, redirecting... Login successfully. Login to apply Login with Facebook Login with Google Login with LinkedIn Logout then Login as Employer Lost Password Lost your password? Please enter your username or email address. You will receive a link to create a new password via email. Manage Applications Manage Jobs Manage Plan Manage Resumes Maximum upload file size: %s Member Member Manage Page Member Options Message Message Box%s Messages Messages Settings Monthly More info More jobs from %s My Profile N/A Name Name* New Company New Job New Job Application New Message New Password New application for job %s Newly registered users will be required to confirm Email before doing any action. Please note that this will delay the register function. Next post No Applications No Bookmarked Jobs No Comments No Companies found No Companies found in Trash No Job Applications found No Job Applications found in Trash No Jobs found No Jobs found in Trash No Resume available No Resumes found No Social Media Link No featured job No jobs found; why not post one? No menu assigned! No results found No results match No resume is publicly viewable/searchable. No saved job alerts Noo Job Count Normal text Not Found. Note Note: Nothing Found Notification Email Number of Application Number of Featured Jobs Old Password is not correct. One Comment Only %d resumes are publicly viewable/searchable. Only 1 resume is publicly viewable/searchable. Only candidate should see this page Only employer should see this page Only logged in employers can view resumes. Only paid employers can view resumes. Only the following file types are allowed: %s Oops! We could not find anything to show you. Open link in a new window Optional Or Ordered list Others Settings Outdent Packages Page Pages: Parent Job Application Password Password must be at least six characters long. Password reset is not allowed for this user Password updated successfully. Passwords do not match. Past Pending Percent % ( 1 to 100 ) Phone Number Pinterest Share Plan Please agree with the Terms of use Please agree with the term Please choose a option Please choose a role for your account. Please choose your account type as Employer or Candidate Please confirm your email to complete registration. Please enter a username. Please enter a valid URL. Please enter a valid email address. Please enter your license key to enable updates to %s. Please enter your name Please fix this field. Please let us know who you are to finish the registration Please login before buying Job Package Please provide a valid email address Please select a image file Please type your email address. Please upload CV file or select a resume Please waiting, not exit page. Please write your application message Post Archive by Day:  Post Job Post Resume Post a Job Post a Resume Posted Preview Preview and Finish Preview and submit your job Previous post Primary Color Primary Menu Print Private Message sent successfully Profile Image Publicly Viewable/Searchable Publish Publish Job Published %s job Qualification(s) RSS Read More Recent Register Registration completed. Regular Regular Italic Reject Reject Application Rejected Rejected %s application Related Jobs Remember Me Remove Repeat password Reply Representative for Company Require Email Confirmation Reset Password Response to %s Resume Resume Categories Resume Detail Resume List Page Resume Listing Resume Settings Resume Title Resume needs a title. Resume not found ! Resume saved Resume was deleted successfully. Resumes Resumes Count Retype your password SKU: Save Save Changes Save My Profile Save New Password School name Search Search Company Search Job Search Job Application Search Results Search Results for: Search Resume Search by Candidate Name Search by Education Search by Experience Search by Resume Title &amp; Content Search by Skill Search for a location... Search for: Section Select Select %s Select File Select Image Select Resume Select Some Options Select an Option Select or Upload your File Select or Upload your Image Send Message Send Message to author Send application Sending info, please wait... Set Featured Set Viewable Set it to yes and this resume will be publicly viewable and searchable. Settings Share Share This Post Share on Facebook Share on Google+ Share on LinkedIn Share on Pinterest Share on Twitter Share this job Sharing Title Shop Show Details Show Featured Jobs Show Job Search Sign In Sign Out Sign Up Sign Up Via Facebook Sign Up Via Google Sign Up Via LinkedIn Single Job Six Skill Name Small Someone requested that the password be reset for the following account: Sorry, there has been an error. Sorry, you can't edit this job alert. Sorry, you can't edit this resume. Sorry, you can't post job_alert. Sorry, you can't post resume. Start/end date Starting a conversation when an employer approves/rejects an application. Status Submit Submit Comment Subscribe to stay update Subscription Summary of Skill Summary of Skills Terms and Conditions Page Terms of use Testimonial Thank you for your subscription. The e-mail could not be sent The email address isn't correct. The new password is blank. The number of days that the job listing will be displayed. There is a problem of verifying your email, please try again. There is problem deleting this Job alert There's a problem when processing your data. Please try again or contact Administrator! There's a problem when you editing this Job, please try again or contact the Administrator There's an unknown error. Please retry or contact Administrator. There's an unknown problem. Please reload and retry. This Resume belongs to Candidate This email was already registered, please choose another one. This field is required. This is a place where you can add new job. This is the place where you can edit and view job applications. This job is expired! This page used for "I agree with the Terms of use" on Registration form This site does not allow registration. This username is invalid because it uses illegal characters. Please enter a valid username. This username was registered. Please choose another one. Title To reset your password, visit the following address: Toggle viewable Total Years of Experience Twitter Profile Twitter Share Twitter URL Twitter Username Type Typography Underline Unfortunately! Your resume didn't pass our application round. Unlimited job posting Unlimited posting? Unordered list Unpublish Unpublish Job Unpublished %s job Upgrade Package Upgrade your membership Upload Upload CV Upload Image Upload an additional image for your user profile. Upload your Attachment User not being registered. Username Username or email Username: %s Video URL View View Cart View Company View Job View Job Application View Resume View all jobs in: &ldquo;%s&rdquo; View cart View more View more jobs: %s Viewable Viewable resume was changed successfully. Viewable/Searchable We found %d new jobs that match your criteria. We found %s available job(s) for you Website Weekly Welcome to %s, Withdraw Withdrawed %s application Work Experience Would you like to go somewhere else to find your stuff? Wrong You are You can not add job You can not delete this resume. You can not edit other's profile. You can not edit this resume. You can set %d more job(s) to be featured. Featured jobs cannot be reverted. You can't edit this company You can't edit this profile You can't start a new conversation with this member. You can't start a new conversation. You cannot change expired jobs to featured ones. You do not have sufficient permissions set job to featured! Please check your plan package! You do not have sufficient permissions to access this page. You don't have permission to view resumes. You have almost finished. Preview and submit your job for approval You have already applied for this job You have already had %d viewable resume(s). You have already logged in. Please <a href="#" onclick="%s">refresh</a> page You have no resume for application, please create a new resume first. You have not logged in yet You have successfully applied for %1$s You have taken too long. Please go back and retry. You must agree with our condition. You must agree with this. You must be logged-in to view your message. You need to provide your email! You can not login if your Facebook doesn't share the email. You used up your Job Limit. You've already subscribed. You've applied %s job You've applied for %s jobs You've bookmarked %s job You've bookmarked %s jobs You've made changes, don't forget to save. You've posted %s jobs You've posted a resume: %1$s You've received %s application(s) You've saved %s resume You've saved %s resumes You've set up %s job alerts. It will be sent to "%s" You've set up 0 job alert. You've set up 1 job alert. It will be sent to "%s" Your Name can't be blank. Your alert name Your cover letter/message sent to the employer Your email Your email address is invalid. Click back and enter a valid email address. Your email is verified. Registration is completed. Your first name Your job application has been submitted successfully Your last name Your membership doesn't allow you to view resumes. Your name Your profile is updated successfully Your session is expired or you submitted an invalid form. Your session is expired. Please reload and retry. Your working language Youtube or Vimeo link Z [%1$s] %2$s applied for %3$s [%1$s] New job posted: %2$s [%1$s] New job submitted: %2$s [%1$s] You've submitted job %2$s [%1$s] You've successfully posted a job %2$s [%1$s] Your job %2$s can't be published [%1$s] Your job %2$s has been approved and published [%1$s]Verify your email address [%s] Password Reset about 1 hour ago about 1 minute ago by %s by a guest click here and follow the steps. eg. &quot;Bachelor Degree&quot; for for: hours in the shopping cart keywords over a year ago see your order history x yesterday Project-Id-Version: Noo Jobmonster
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-27 14:52+0700
PO-Revision-Date: 2016-05-27 14:52+0700
Last-Translator: rafa0192 <rafa0192@gmail.com>
Language-Team: 
Language: es_MX
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.7
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: ..
X-Loco-Target-Locale: es_MX
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
  para %s % % (1 al 100) %1$s Ha publicado un empleo:<br/><br/>
				<a href="%2$s">Ver Detalle de Empleo</a>.
				<br/><br/>
				Atentamente,<br/>
				%3$s %1$s acaba de presentar un trabajo para su revisión.<br/><br/>
				<a href="%2$s">Ver detalle de Empleo</a>.<br/><br/>

				Atentamente,<br/>
				%3$s %1$s ha respondido a su aplicación ( %d ) Solicitudes ( %d ) visitas %d+ Nuevos Empleos - Alertas de Empleos de %s %s Licencia  %s día(s) %s days ago %s Empleos Destacados %s ha publicado %s empleos Hace %s horas %s  Publicaciones de Empleo %s empleos Hace %s minutos %s publicado <a href="%s">View</a> %s salvado. %s prevista para: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Vista anticipada</a> Hace %s segundos %s enviado. <a target="_blank" href="%s">Preview</a> (Opcional) (opcional) ** Categoría de trabajo: %s ** Tipo de Empleo: %s ** Ubicación: %s + Añadir nueva ubicación - Seleccione - - Seleccione un Candidato - - Seleccione una categoría - - Seleccione una ubicación - - Seleccione un Empleador - -Todos los Empleos- -Granel de Acciones- -Seleccionar- ... <h3>
Requisitos del Empleo
</h3>
<ol><li>
Requisito detallado para la vacante.?
</li><li>
Requisito detallado para la vacante.?
</li><li>
Requisito detallado para la vacante.
?</li></ol> A Un 
Empleo Una Publicación Un Producto Un Currículum ACCIÓN ACCIÓN: Acciones Activar Activo Añadir Añadir Empresa Añadir Educación Añadir Experiencia Añadir Imágenes Añadir Empleo Añadir solicitud de Empleo Agregar Nueva Añadir Nueva Solicitud Añadir nuevo empleo Añadir Nueva 
Ubicación de Empleo Añadir Habilidad DIRECCIÓN Nombre de alerta Todos Todos los %s Todos los candidatos Todas Las Categorías Todas las Empresas Todos los Empleadores Todas las Ubicaciones Todos los Mensajes%s Todos los tipos Todo listo , diviertase. Todos los Empleos Archivos Permitidos Archivos Permitidos: %s ¿Ya tienes una cuenta?  Anuncios Anuncio%s Solicitud Solicitud a través de 
"%s" on %s Fecha de Solicitud Empleo Solicitado Mensaje de Solicitud de Empleo Solicitar 
Empleo Aplicar Empleo a través de LinkedIn Aplicar para este empleo Aplicar a través de LinkedIn Aplicar con LinkedIn Aprobar Aprobar Solicitud Mensaje de Aprobado/Rechazado Aprobado %s Solicitud Aprobada Solicitudes ¿Seguro que quieres eliminar este Empleo? Autor Puestos de trabajo disponibles Atrás Perfil de Behance Atentamente, CUMPLEAÑOS Blog Negrita Fina Fina Cursiva Marcar Empleo El marcador ha sido retirado. Retirar el marcador. Marcadores Explorar CV 
| Archivo Adjunto No se puede encontrar este paquete. No se puede encontrar este empleo. No se puede encontrar este 
Currículum No se puede obtener el correo electrónico de su cuenta de redes sociales Cancelar Los candidatos pueden iniciar una conversación con Candidato Información Básica del Candidato Perfil Social del 
Candidato  Nombre del Candidato El Candidato necesita un correo electrónico válido. Mensaje del candidato Los candidatos que han aplicado a sus puestos de Empleo Subtotal del Carrito Categoría Modificar Archivo Cambiar Imagen Cambiar La Contraseña Cambiar perfil Los cambios se han guardado correctamente. Revise su correo electrónico para el enlace de confirmación. Elija los valores de diseño para su Lista de trabajos Seleccione un paquete Elegir un paquete que se adapte a su necesidad selecciones un paquete Borrar archivo Borrar Galería Borrar Imagen Haga clic aquí para cancelar la respuesta Cierre Fecha de Cierre Navegación de Comentarios Los comentarios están cerrados. Listado de Empresa(s) Listado de Empresa(s) Empresa Descripción De La Compañía Información De La Empresa Logo De La Compañía Nombre De La Empresa Perfil de La Empresa Página Web De La Compañía Empresa actualizada Completa la configuración ahora. Confirmar nueva contraseña ¡Felicidades!
 %1$s,<br/><br/>

Usted ha solicitado con éxito para
 %2$s.<br/>
<a href="%3$s">Ver detalles de empleo</a><br/>

Puede gestionar y seguir el estado de los puestos de empleos y 
solicitudes 
aplicadas en
 <a href="%4$s">
Mis Solicitudes 

</a>.
<br/><br/>

Nota: Debido al alto volumen de solicitudes, los empleadores no pueden ser capaces de responder a todas las aplicaciones.

<br/><br/>

Buena suerte en su futuro profesional!

<br/><br/>

Atentamente,<br/>
%5$s ¡Enhorabuena! Hemos recibido su solicitud para el empleo y encontramos sus habilidades y experiencia , cumplen con nuestros requisitos. Nos pondremos en contacto contigo en breve para el detalle de la segunda ronda de selección.
Atentamente. ¡Enhorabuena! Usted ha creado con éxito cuenta en 
[%1$s] ¡Enhorabuena! Su Currículum pasó nuestra ronda de solicitudes. Síguenos en Conéctate con nosotros en nuestro Facebook Conéctate con nosotros en nuestro Google Plus Conéctate con nosotros en nuestro instagram Conéctate con nosotros en nuestro LinkedIn  Conéctate con nosotros en nuestro Twitter Conéctate con nosotros en nuestro sitio Web Email De Contacto Continuar Sigue Leyendo No se pudo añadir nueva solicitud de empleo No se pudo registrar ... por favor póngase en contacto con el administrador del sitio Imagen de Portada Crear Nuevo Crear nuevo Currículum Crear nueva alerta de empleo Usuario creado exitosamente. Empleo Actual EMPRESA ACTUAL EMPLEO ACTUAL Contraseña Actual Enlace de Solicitud Personalizado Enlace personalizado para redirigir los solicitantes de empleo a la hora de aplicar para este trabajo. Diario Fecha de Activación FECHA DE MODIFICACIÓN Estimado %1$s,<br/>
Gracias por registrar una cuenta en %2$s como candidato usted puede iniciar la búsqueda de sus puestos de trabajo previstos o crear su CV ahora.
<br/><br/>
Atentamente, <br/>
%3$s Estimado 
%1$s,<br/>

Gracias por registrar una cuenta en %2$s como empleador. Usted puede empezar a publicar trabajos o buscar sus posibles candidatos ahora.
<br/><br/>

Atentamente,
<br/>
%3$s Estimado %s, Estimado,<br/>
Gracias por registrar una cuenta en %1$s. Por favor <a href="%2$s">Haga clic aquí</a>  o utilice el siguiente para confirmar su Correo Electrónico.<br/>
%3$s
<br/><br/>
Saludos Cordiales,<br/>
%4$s Borrar Borrar Sulicitud Eliminar Empleo Eliminar Alerta de Empleo Eliminar Curriculum Eliminar 
%s solicitud Eliminado %s empleo Describa su compañía y vacante Describa su trabajo en unos pocos párrafos Describa su curriculum Directorio Desactivar Desactivar visible Mostrar las Categorías para los currículum. Muestra el número total de puestos de trabajo disponibles. Muestra el número total de currículums disponibles. Quieres continuar con esta opción? ¿No tienes una cuenta todavía?
 <a href="%s" class="member-register-link" >
Regístrate Ahora
 <i class="fa fa-long-arrow-right"></i></a> Descargar  Descargar mi CV Editar Editar empresa Editar Información de la empresa Editar Galería Editar HTML Editar Empleo Editar Alerta de Empleo Editar Solicitud de Empleo Editar alerta de empleo Editar Perfil Editar Currículum Educación Email Enviar correo al Candidato Frecuencia de Correo Electronico Este correo ya existe en nuestro sistema Correo electrónico para recibir la notificación de la solicitud. Dejar en blanco para utilizar el perfil de correo electrónico del empleador. Correo electrónico para recibir la notificación de la solicitud. Dejar en blanco para usar su cuenta de correo electrónico. Correo Electronico* Empleador Información del empleador Mensaje del Empleador El mensaje de Empleador Los empleadores pueden iniciar conversación con Los empleadores que aprobaron o rechazaron sus aplicaciones. Los empleadores que aprobaron sus aplicaciones. Activar Introduzca un título breve para su Vacante Introduzca un nombre de usuario o dirección de correo electrónico. Introduzca las palabras clave para que coincida con los empleos Ingrese nueva ubicación Escriba el texto de la Caja. Escriba su descripción de la empresa Escriba el nombre de su empresa Introduzca el sitio web de su Empresa Ingrese su Email aquí ... Error en la eliminación. Error de publicación. Error de despublicación Error al aprobar la aplicación. Error al eliminar la aplicación. Error al rechazar la aplicación. Error al retirar la aplicación. Todos ( Todos los Candidatos y Empleadores ) Ttodos(Todos los candidatos y empleadores) Nivel de empleo esperado Empleo Expirado Empleo Expirado Los anuncios expirados serán eliminados de la vista pública. Extra ligero Fina cursiva Perfil de Facebook Compartir en Facebook Facebook URL Destacado Empleos Destacados Empleos Destacados ¿Destacar? Filtrar: Nombre Cinco Has Olvidado Tu Contraseña? Quincenal Frecuencia NOMBRE COMPLETO Información General Información General Opciones Generales Ir Google + URL Compartir en Google+ Titulo 1 Titulo 2 Titulo 3 Titulo 4 Titulo 5 Titulo 6 Hola %1$s,
<br/><br/>

Ha publicado un Currículum
:<br/>

Título:
 %2$s<br/>

Ubicación:
 %3$s<br/>

Categoría:
 %4$s<br/>
<br/><br/>

Usted puede administrar sus 
Currículum 
en
 <a href="%5$s">Administrar Currículum</a>.
<br/><br/>

Atentamente
,<br/>
%6$s Hi %1$s,<br/>
				<br/>
				%2$s Ha aplicado para %3$s.<br/>
				<a href="%4$s">Ver CV</a><br/>
				Usted puede manejar las solicitudes para sus empleos en <a href="%5$s">Solicitudes</a>.
				<br/><br/>
				Atentamente,<br/>
				%6$s Hola %1$s,<br/>
%2$s 
acaba de dar respuesta a su solicitud de empleo
 <a href="%3$s">%4$s</a> 
con el mensaje
: 
<br/>
<div style="font-style: italic;">
%5$s
</div>
<br/>

Usted puede administrar sus Solicitudes en
<a href="%6$s">
Administrar Solicitud
</a>.
<br/>
Atentamente,<br/>
%7$s Hi %1$s,<br/><br/>
				Usted ha enviado un nuevo empleo<br/>
				<a href="%2$s">Ver detalles de Empleo</a>.
				<br/><br/>
				Vamos a revisar su entrada de empleo y responderemos de nuevo a usted pronto.<br/>
				Puede gestionar y seguir el estado de los empleos in <a href="%3$s">Manejar Empleos</a><br/><br/>
				Atentamente,<br/>
				%4$s Hi %1$s,<br/><br/>
				Usted ha publicado con éxito un nuevo empleo:<br/>
				<a href="%2$s">Ver detalles de Empleo</a>.
				<br/><br/>
				Puede administrar los empleos en <a href="%3$s">Administrar Empleos</a><br/><br/>
				Best regards,<br/>
				%4$s Hola %1$s,<br/><br/>
Su solicitud de empleo %2$s no puede ser publicada y se ha eliminado. Usted tendrá que solicitar otro Empleo.
<br/><br/>
Usted puede manejar sus puestos de empleo en <a href="%3$s">Manejar Empleos</a><br/><br/>
Atentamente,<br/>
%4$s Hi %1$s,<br/><br/>

Su empleo enviado
 %2$s ha sido publicado y aprobado %3$s:<br/>
<a href="%4$s">Ver detalle de Empleol</a>.
<br/><br/>
Puede Manejar sus empleos en <a href="%5$s">Manejar Empleos</a><br/><br/>

Atentamente
,<br/>
%6$s Hola,
Hemos recibido su solicitud para el trabajo y encontramos sus habilidades y experiencia no coincide con nuestros requisitos. Gracias por su interés en nuestra oferta de empleo y buena suerte en su futuro profesional. Hola, Ocultar Detalles Mayor Nivel de Grado Estoy de acuerdo con las Estoy muy interesado en la posición %s en %s. Creo que mis habilidades y experiencia laboral me hacen un candidato ideal para este papel. Espero poder hablar con usted pronto acerca de esta posición. gracias por tu consideración.
Atentamente
 Soy un candidato en busca de un Empleo. Soy un empleador que busca contratar ID: %d Si esto fue un error, simplemente ignorar este mensaje y no pasará nada. En esta sección usted tiene ajustes para su página de lista de Registros, la página de Archivos y la página individual de empleos. En esta sección usted tiene ajustes para su Lista 
Currículums
. Inactivo Sangrar Información Insertar Imagen Insertar Enlace Perfil de Instagram Instagram URL Preséntate
 a 
ti mismo Email Inválido Confirmación no válido de código, por favor ingrese su código de nuevo. Empleo
 no válido Nombre de usuario o e-mail no 
válido
. Cursiva Empleo Empleo Agregados Alerta De Empleo La Alerta de Empleos necesita un nombre. Alertas Solicitudes De Empleo Configuración de Aplicación de empleos Solicitudes De Empleos Categoría de Empleo Descripción del Empleo Detalles de Empleo Duración Duración Empleo (días) Limite de Empleos Lista de 
Empleos Lista de diseño para 
Empleos Lista de Empleos Sidebar de Empleos Ubicación del Empleo Paquete de Empleo Etiqueta para este empleo Título Tipo de Empleo Alerta de Empleo eliminada Alerta de Empleo Salvada Borrador de Solicitud de empleo actualizado. Solicitud de empleo publicada. Solicitud de Empleo Guardada Solicitud de Empleo Enviada. Solicitud de empleo actualizada. Empleo
 Solicitado Para Empleo Marcado Contador de Empleo Empleo presentado por %s dias Empleo no encontrado! Empleo guardado Los solicitantes de empleo pueden encontrar su trabajo y en contactarse con usted por correo electrónico o 
%s con respecto a sus posibilidades de aplicación. Vista previa de toda su información a fondo antes de presentar su empleo para su aprobación. Empleo establecido como destacado, fue logrado con éxito. Condición de envío de Empleo Empleo
 agregado con éxito Empleo
 Actualizado Fecha de Expiración del Empleo Empleos Palabra Clave Palabras clave Idioma Apellido Resultados Deja una respuesta a %s Que piensas Licencia License <a target="_blank" href="%s" style="">Como obtener una licencia?</a> Email de licencia o Dirección de Dominio Llave de Licencia Llave de Licencia: Fina Fina cursiva Perfil de Linkedin Compartir en Linkedln Linkedin URL Cargar Más Cargando Mapas Ubicación Conectado como 
<a href="%1$s">%2$s</a>. <a href="%3$s" title="
Salir de esta cuenta
">
Cerrar Sesión
?</a> Iniciar Sesión Entrar como Empleador Ingresa o crea una cuenta Ingreso fue exitoso, redireccionamiento en curso... Login exitoso Inicia sesión para Aplicar Iniciar Sesión con Facebook Iniciar Sesión con Google Inicio de sesión con LinkedIn Salir a y Entrar como Empleador Contraseña Perdida Perdiste tu contraseña? Introduzca su nombre de usuario o la dirección de correo electrónico. Usted recibirá un enlace para crear una nueva contraseña por correo electrónico. Solicitudes Mis Puestos Planes Mi Currículum Tamaño máximo de archivo de carga: % s Miembro Pagina de Administración de Miembro Opciones de miembros Mensaje Buzón%s Mensajes Ajustes Mensual Más... Más 
Empleo
 de % s Mi Perfil N/A Nombre Nombre* Nueva empresa Nuevo Empleo Nueva solicitud de empleo Nuevo Mensaje NUEVA CONTRASEÑA Nueva aplicación para el empleo %s Todos los nuevos usuarios registrados requerirán una confirmacion de correo electrónico antes de realizar cualquier acción. Tenga en cuenta que esto retrasará la función de registro. Proximo Post No existen solicitudes No existen empleos marcados Sin Comentarios No hay empresas encontradas No se encuentran en la Papelera Empresas No hay solicitudes de empleo encontradas No hay solicitudes de empleo en la papelera No hay trabajos encontrados No se encuentran empleos en la Papelera Ningún Currículum disponible No se encontro ningún CV disponible Ningún Vinculo de Redes Sociales Sin Destacados Aún
 no tienes puestos de empleos publicados , porque no empiezas y publicas el primero ? Ningún menú asignado No se encontraron resultados Ningún resultado coincide con su búsqueda Ningún currículum es visible para el publico / búsquedas. No existe ninguna Alerta de Empleo guardada Contador de Empleo Texto Normal No Encontrado Nota Nota: No se ha encontrado nada Solicitud de Notificarme por correo electrónico Numero de Solicitudes Número de Empleos Destacados La antigua contraseña no es correcta. Un Comentario Solamente los currículos %d son públicos y visibles / búsquedas. Sólo 1 
currículum
 se puede ver públicamente / búsquedas. Sólo el candidato debe ver esta página Sólo el empleador debe ver esta página Únicamente los empleadores pueden ver a los candidatos. Solamente los empleadores pagos pueden ver los Candidatos Sólo se permiten los siguientes tipos de archivo: % s ¡Huy! No pudimos encontrar nada que mostrar a usted. Abrir enlace en nueva ventana Opcional O Lista Ordenada Otros ajustes Desangrar Paquetes Página Páginas: Solicitud de Empleo Matriz Contraseña La contraseña debe tener 6 caracteres de longitud. Restablecimiento de contraseña no está permitido para este usuario Contraseña actualizada correctamente. Las contraseñas no coinciden. Anterior Pendiente Porcentaje % (1 a 100) Número Telefónico Compartir en Pinterest Plan Por favor, estar de acuerdo con las condiciones de uso Por favor, estar de acuerdo con el término Por favor elige una opción Por favor, elija un rol para su cuenta. Por favor escoja su tipo de cuenta Empleador o Candidato Por favor confirme su correo electrónico para completar el registro. Por favor, introduzca un nombre de usuario. Por favor, introduce una dirección válida. Por favor, introduce una dirección de correo electrónico válida. Introduzca una llave de licencia para poder obtener actualizaciones de este tema %s. Por favor, escriba su nombre Este campo es requerido Por favor háganos saber quién es usted para terminar el registro Por favor, iniciar sesión antes de comprar el paquete de empleo Proporcione una dirección de correo electrónico válida Por favor, seleccione un archivo de imagen Por favor escriba su dirección de correo electrónico. Por favor, sube archivo CV o seleccione un currículum Por favor espere , no salga de esta pagina Por favor escriba su mensaje para esta solicitud. Entrada hecha por Día: Publicar Empleo Publicar CV Publicar Empleo Publicar un CV Publicado Vista Anticipada Vista previa y Finalizar Obtenga Una vista previa y envie su Empleo Post anterior Color Primario Menú Principal Imprimir Mensaje privado enviado exitosamente Imagen del Perfil Visible al público / 
 búsquedas Publicar Publicar Empleo %s Empleo Publicado Titulo Profesional RSS Leer Más Reciente Registrarse Registro Completado  ◕‿◕ Regular Cursiva Regular Rechazar Rechazar Solicitud No Aprobada %s Solicitud No Aprobada Empleos Relacionados Recuérdame Eliminar Repita la contraseña Responder Representante de la Empresa Requiere de una Confirmación de Correo Electronico Restablecer La Contraseña Respuesta a %s Candidato Categorías de 
Currículum. Detalle del Curriculum Lista de Currículums Listado de Candidatos Ajustes Curriculum Título de Currículum El Currículum necesita un título. Curriculum no encontrado! Currículum salvado Currículum eliminado correctamente. Candidatos Contador de Curriculum Vuelva a escribir su contraseña Código de referencia: Guardar Guardar Cambios Guardar mi perfil Guardar nueva contraseña Nombre de la Escuela / u
niversidad / 
Institución
 Educativa Buscar Buscar Empresa Búsqueda de empleo Buscar Solicitud de Empleo Resultados de la Búsqueda Resultados encontrados para: Buscar Currículum Búsqueda por Nombre del candidato Búsqueda por Educación Búsqueda por Experiencia Búsqueda por 
Título del 
Currículum 
 &amp; Content Búsqueda por Habilidad Buscar una Ubicación... Buscar: Sección Seleccionar Seleccione %s Seleccione Archivo Seleccionar Imagen Seleccionar Currículum Seleccione Algunas Opciones Seleccione una opción Seleccionar o cargar el archivo Seleccionar o Cargar su imagen Enviar Mensaje Enviar Mensaje al autor Enviar Solicitud Enviando información, por favor espere ... Establecer como Destacados Establecer visible Selecciones (SI) y éste CV estará públicamente y se puede ver y buscar públicamente. Ajustes Compartir Compartir esta Publicación Compartir en Facebook Compartir en Google+ Compartir en Linkedln Compartir en Pinterest Compartir en Twitter Comparte este empleo Titulo Compartido Tienda Mostrar Detalles Mostrar 
Empleos
 Destacados Mostrar Búsqueda de Empleo Ingresar Desconectarse Registrarse Inscribirse a través de Facebook Inscribirse a través de Google Inscribirse a través de LinkedIn Un Empleo Seis Nombre de 
Habilidad  Pequeña Alguien pidió que la contraseña se restablecerá a la siguiente cuenta: Lo sentimos,  se ha producido un error. Lo sentimos, no se puede editar esta alerta de empleo. Lo sentimos, no se puede editar este currículum. Lo sentimos, no se puede publicar job_alert. Lo sentimos, no se puede publicar un Curriculum. Fecha de Inicio / Fin Iniciar una conversación cuando un empleador aprueba / rechaza una solicitud. ESTADO Enviar Enviar Comentario Suscríbete para mantenerte al día Suscripción Resumen de Habilidad Resumen de Habilidades Página de Términos y Condiciones de Uso Condiciones de uso Testimonios Gracias por su suscripción. El correo no pudo ser enviado La dirección de correo electrónico no es correcta. La nueva contraseña está en blanco. El número de días que se mostrará la lista de Empleo. Ocurrio un problema al verificar su correo electrónico, por favor intente de nuevo Hubo un problema al eliminar esta alerta de empleo Hay un problema al procesar sus datos. Inténtelo de nuevo o póngase en contacto con administrador! Hay un problema con la edición de este empleo, por favor intente de nuevo o póngase en contacto con el Administrador Hay un error desconocido. Por favor, vuelva a intentarlo o contacte al Administrador  Hay un problema desconocido. Vuelve a cargar y vuelva a intentarlo. Este Currículum pertenece al Candidato Este correo electrónico ya está registrado, por favor elija otro. Este campo es requerido Aquí es donde usted puede añadir un nuevo empleo. Aquí es donde se puede editar y ver las solicitudes de empleo. Empleo Expirado Esta página se utiliza para aceptar  que "Estoy de acuerdo con los términos de uso" en el formulario Actualmente el Registro no esta permitido. Este nombre de usuario no es válido, ya que utiliza caracteres no válidos. Por favor, introduzca un nombre de usuario válido. Este nombre de usuario ya está registrado. Por favor elija otro. Título Para restablecer su contraseña, visite la siguiente dirección: Alternar Visible Experiencia Total  en Años Perfil de Twitter Compartir en Twitter Twitter URL Nombre De Usuario De Twitter Tipo Tipografía Subrayar Por desgracia! Su currículum no pasó nuestra ronda de solicitudes. Publicaciones Ilimitadas Publicación ilimitada Lista Desordenada Despublicar Despublicar Empleo %s empleo Despublicado Actualizar Paquete Mejora tu membresía Subir Cargar al CV Cargar Imagen Suba una imagen adicional para su perfil de usuario. Sube tu Curriculum Usuario No registrado Nombre de usuario Nombre de usuario o correo electrónico Nombre de usuario: % s Enlace de Video Ver Ver Carrito Ver la empresa View Job Ver Solicitud de Empleo Ver Currículum Ver todos los empleos con la etiqueta: &ldquo;%s&rdquo; Ver Carrito Ver más Ver más empleos: %s Visible Currículum visible , Cambio fue un éxito Visible / búsquedas Encontramos %d nuevos empleos correspondiente a sus criterios. Encontramos %s empleo(s) disponibles para usted Sitio Web Semanal Bienvenido a %s, Retirar %s Ha sido Retirada la solicitud Experiencia Laboral ¿Le gustaría ir a otro lugar donde encontrar tus cosas.? Algo salió mal. Eres No puedes agregar Empleo , verifique su plan y sus limites de entradas. No se puede eliminar este currículum. No puede editar este perfil No puede editar este Currículum. Puede establecer %d más Empleo(s) destacados. Trabajos destacados no pueden ser revertidos. No puede editar esta empresa No puede editar este perfil No se puede iniciar una nueva conversación con este miembro No se puede iniciar una nueva conversación . No se puede cambiar a destacado un Empleo expirado Usted no tiene suficientes permisos establecidos para usar la opción destacado! ,Por favor verifique su plan! Usted no tiene permisos suficientes para acceder a esta página. Usted no tiene permiso de ver curriculums Casi ha terminado. Obtenga una vista previa y envie su empleo para su aprobación Usted ya ha solicitado para este 
Empleo Usted ya tiene %d currículum visible (s). Usted ya está en linea. Por favor, 
<a href="#" onclick="%s">refresh</a> page Usted no tiene ningún Curriculum para su aplicación, por favor crear uno. Aún no has iniciado sesión Ha solicitado con éxito el puesto de 
%1$s Esto ha tomado demasiado tiempo. Por favor, vaya atrás y vuelva a intentarlo. Usted debe estar de acuerdo con nuestros términos de uso. Usted debe estar de acuerdo con esto. Usted debe ser iniciar sesión para ver el mensaje. Es necesario proporcionar su correo electrónico! No se puede iniciar sesión si su Facebook no comparte el correo electrónico. Ya utilizó su límite de empleos permitidos en su paquete. Ya se ha suscrito. Usted ha aplicado %s Empleo Usted ha aplicado para %s Empleos Usted ha marcado %s  empleos Usted ha marcado %s  empleos Usted ha hecho cambios, no se olvide de guardar. Usted ha publicado 
 %s empleos Usted ha publicado un Currículum: %1$s Has recibido 
%s solicitud(s) Has salvado %s currículums  Has salvado %s currículums  Usted ha configurado %s alertas de empleos. Será enviado a "% s" Usted ha configurado 
0 
alertas empleo. Usted ha configurado 
1
 alerta de empleo. Será enviado a "%s" Su nombre no puede estar en blanco. Tu nombre de alerta Su carta de presentación / mensaje enviado al empleador Su email Su dirección de correo electrónico no es válida. Haga clic de nuevo e introduzca una dirección de correo electrónico válida. Su correo electrónico se verifico. El registro se ha completado. Su nombre Su solicitud de empleo ha sido enviada correctamente Su Apellido Su membresía no le permite ver los Candidatos Su Nombre Su perfil se ha actualizado correctamente Su sesión ha caducado o ha presentado unos credenciales inválidos. Su sesión ha caducado. Vuelve a cargar y vuelva a intentarlo. Su idioma principal de trabajo Vídeo de introducción (Youtube o Vimeo) Z [%1$s] %2$s 
ha aplicado
 para %3$s [%1$s] 
Nuevo empleo publicado: 
%2$s [%1$s] 
Nuevo trabajo solicitado: 
%2$s [%1$s] Se ha enviado el empleo %2$s [%1$s] 
Usted ha publicado éxitosamente un Empleo 
%2$s [%1$s] Su Empleo %2$ s no puede publicarse [%1$s] 
Su puesto de Empleo %2$s ha sido aprobado y publicado [%1$s]Verificar su dirección de Correo Electronico. [%s] 
Restablecer contraseña Hace alrededor de 1 hora Hace alrededor de 1 minuto Por %s Por un Cliente Haga clic aquí y siga los pasos. eg. &quot;
Grado De Bachiller
&quot; para para: horas en el carrito de compras Palabras clave hace más de un año ver su historial de pedidos x ayer 