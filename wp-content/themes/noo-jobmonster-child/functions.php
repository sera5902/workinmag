<?php
/**
 * Theme functions for NOO JobMonster Child Theme.
 *
 * @package    NOO JobMonster Child Theme
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

// If you want to override function file ( like noo_job.php, noo_resume.php ... ),
// you should copy function file to the same folder ( like /framework/admin/ ) on child theme, then use similar require_one 
// statement like this code.
// require_once dirname(__FILE__) . '/framework/admin/noo_job.php';

add_action('admin_print_footer_scripts', 'my_action_javascript', 99);
function my_action_javascript() {
    ?>
    <script>
    jQuery(document).ready(function($) {

        jQuery('<span class="send_mail_admin">Отправить</span>').appendTo('.noo-form-group._canseled .noo-control ');
        
        $("._canseled").on("click",".send_mail_admin", function(){
            var prichina = jQuery('select#_canseled').val();
            var autor_id = jQuery('select#post_author_override').val();
            var data = {
                action: 'my_action',
                whatever: prichina,
                autor_id: autor_id
            };

            // с версии 2.8 'ajaxurl' всегда определен в админке
            jQuery.post( ajaxurl, data, function(response) {
                alert('Сообщение отправленно!');
            });
        });
        
    });
    </script>
    <style>
    ._canseled .noo-control {
        position: relative;
    }
    .send_mail_admin {
        top: 1px;
        position: absolute;
        right: -99px;
        padding: 0px 10px;
        background: #84bd04;
        color: #fff;
        border-radius: 3px;
        cursor: pointer;
        transition: .2s;
    }
    .send_mail_admin:hover {
        background: #99d219;
    }
    </style>
    <?php
}

add_action( 'wp_ajax_my_action', 'my_action_callback' );
function my_action_callback() {

    $whatever = $_POST['whatever'];
    $autor_id = $_POST['autor_id'];
    
    $emailTo = $autor_id;
    $user = get_userdata($autor_id);
    $user_mail = $user->user_email;
    $subject = "Ваша вакансия отклонена модерацией";

    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
    mail($user_mail, $subject, $whatever, $headers); 
    

    wp_die(); // выход нужен для того, чтобы в ответе не было ничего лишнего, только то что возвращает функция
}

function is_user_has_role( $role, $user_id = null ) {
    if ( is_numeric( $user_id ) ) {
        $user = get_userdata( $user_id );
    }
    else {
        $user = wp_get_current_user();
    }
    if ( !empty( $user ) ) {
        return in_array( $role, (array) $user->roles );
    }
    else
    {
    	return false;
    }
}

// $has_role = is_user_has_role("employer");
// $has_role_2 = is_user_has_role("candidate");
// if($has_role || $has_role_2)
// {
// 	add_filter('show_admin_bar', '__return_false');
// }

add_action('admin_init', 'my_general_section');  
function my_general_section() {  
    add_settings_section(  
        'my_settings_section', // Section ID 
        'Публикация постов в Вконтакте', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'option_1', // Option ID
        'Option 1', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'option_1' // Should match Option ID
        )  
    ); 

   

    register_setting('general','option_1', 'esc_attr');
}

function my_section_options_callback() { // Section Callback
    echo '<p>Если есть какой-то текст он будет выводится при публикации вакансии.</p>';  
}

function my_textbox_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" style="width: 100%" />';
}

?>