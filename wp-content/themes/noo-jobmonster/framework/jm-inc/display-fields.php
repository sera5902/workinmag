<?php
if ( ! function_exists( 'noo_display_field' ) ) :
	function noo_display_field( $field = array(), $field_id = '', $value = '' ) {
		if( empty( $value ) ) return;

		$blank_field = array( 'label' => '', 'type' => 'text' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;
		switch ( $field['type'] ) {
			case "textarea":
				noo_display_textarea_field( $field, $field_id, $value );
				break;
			case "select":
				noo_display_select_field( $field, $field_id, $value );
				break;
			case "multiple_select":
				noo_display_multiple_select_field( $field, $field_id, $value );
				break;
			case "radio" :
				noo_display_radio_field( $field, $field_id, $value );
				break;
			case "checkbox" :
				noo_display_checkbox_field( $field, $field_id, $value );
				break;
			case "text" :
				noo_display_text_field( $field, $field_id, $value );
				break;
			case "datepicker" :
				noo_display_datepicker_field( $field, $field_id, $value );
				break;
			default :
				do_action( 'noo_display_field_' . $field['type'], $field, $field_id, $value );
				break;
		}
	}
endif;

if ( ! function_exists( 'noo_display_text_field' ) ) :
	function noo_display_text_field( $field = array(), $field_id = '', $value = '' ) {
		$label = isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'];
		$value = noo_convert_custom_field_value( $field, $value );
		?>
		<h3 class="label-<?php echo $field_id; ?>"><?php esc_html_e($label)?></h3>
		<p class="value-<?php echo $field_id; ?>"><?php echo do_shortcode( $value ); ?></p>
		<?php
	}
endif;

if ( ! function_exists( 'noo_display_textarea_field' ) ) :
	function noo_display_textarea_field( $field = array(), $field_id = '', $value = '' ) {
		$label = isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'];
		$value = noo_convert_custom_field_value( $field, $value );
		?>
		<h3 class="label-<?php echo $field_id; ?>"><?php esc_html_e($label)?></h3>
		<p class="value-<?php echo $field_id; ?>"><?php echo esc_html( $value ); ?></p>
		<?php
	}
endif;

if ( ! function_exists( 'noo_display_radio_field' ) ) :
	function noo_display_radio_field( $field = array(), $field_id = '', $value = '' ) {
		$label = isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'];
		$value = !is_array( $value ) ? noo_json_decode( $value ) : $value;
		$value = noo_convert_custom_field_value( $field, $value );
		?>
		<h3 class="label-<?php echo $field_id; ?>"><?php esc_html_e($label); ?></h3>
		<?php foreach ($value as $v) : ?>
			<div class="value-<?php echo $field_id ?> <?php echo $v ?>">
				<i class="fa fa-check-circle"></i>
				<?php echo esc_html_e($v); ?>
			</div>
		<?php endforeach;
	}
endif;

if ( ! function_exists( 'noo_display_checkbox_field' ) ) :
	function noo_display_checkbox_field( $field = array(), $field_id = '', $value = '' ) {
		noo_display_radio_field( $field, $field_id, $value );
	}

endif;

if ( ! function_exists( 'noo_display_select_field' ) ) :
	function noo_display_select_field( $field = array(), $field_id = '', $value = '' ) {
		noo_display_text_field( $field, $field_id, $value );
	}
endif;

if ( ! function_exists( 'noo_display_multiple_select_field' ) ) :
	function noo_display_multiple_select_field( $field = array(), $field_id = '', $value = '' ) {
		$label = isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'];
		$value = !is_array( $value ) ? noo_json_decode( $value ) : $value;
		$value = noo_convert_custom_field_value( $field, $value );
		$value = implode(', ', $value);
		?>
		<h3 class="label-<?php echo $field_id; ?>"><?php esc_html_e($label)?></h3>
		<p class="value-<?php echo $field_id; ?>"><?php echo esc_html( $value ); ?></p>
		<?php
	}
endif;

if ( ! function_exists( 'noo_display_datepicker_field' ) ) :
	function noo_display_datepicker_field( $field = array(), $field_id = '', $value = '' ) {
		$label = isset( $field['label_translated'] ) ? $field['label_translated'] : $field['label'];
		$value = noo_convert_custom_field_value( $field, $value );
		?>
		<h3 class="label-<?php echo $field_id; ?>"><?php esc_html_e($label)?></h3>
		<p class="value-<?php echo $field_id; ?>"><?php echo esc_html( $value ); ?></p>
		<?php
	}
endif;
