<?php

if( !function_exists( 'jm_admin_job_updated_messages' ) ) :
	function jm_admin_job_updated_messages($messages){
		global $post, $post_ID, $wp_post_types;
		
		$messages['noo_job'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( __( '%s updated. <a href="%s">View</a>', 'noo' ), $wp_post_types['noo_job']->labels->singular_name, esc_url( get_permalink( $post_ID ) ) ),
			2 => __( 'Custom field updated.', 'noo' ),
			3 => __( 'Custom field deleted.', 'noo' ),
			4 => sprintf( __( '%s updated.', 'noo' ), $wp_post_types['noo_job']->labels->singular_name ),
			5 => isset( $_GET['revision'] ) ? sprintf( __( '%s restored to revision from %s', 'noo' ), $wp_post_types['noo_job']->labels->singular_name, wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( __( '%s published. <a href="%s">View</a>', 'noo' ), $wp_post_types['noo_job']->labels->singular_name, esc_url( get_permalink( $post_ID ) ) ),
			7 => sprintf( __( '%s saved.', 'noo' ), $wp_post_types['noo_job']->labels->singular_name ),
			8 => sprintf( __( '%s submitted. <a target="_blank" href="%s">Preview</a>', 'noo' ), $wp_post_types['noo_job']->labels->singular_name, esc_url( add_query_arg( 'job_id', $post_ID, Noo_Member::get_endpoint_url('preview-job') ) ) ),
			// 8 => sprintf( __( '%s submitted. <a target="_blank" href="%s">Preview</a>', 'noo' ), $wp_post_types['noo_job']->labels->singular_name, esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
			9 => sprintf( __( '%s scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview</a>', 'noo' ), $wp_post_types['noo_job']->labels->singular_name,
				date_i18n( __( 'M j, Y @ G:i', 'noo' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
			10 => sprintf( __( '%s draft updated. <a target="_blank" href="%s">Preview</a>', 'noo' ), $wp_post_types['noo_job']->labels->singular_name, esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		); 
		
		return $messages;
	}
	add_filter( 'post_updated_messages', 'jm_admin_job_updated_messages' );
endif;

if( !function_exists( 'jm_admin_job_edit_title_placeholder' ) ) :
	function jm_admin_job_edit_title_placeholder($text, $post){
		if ( $post->post_type == 'noo_job' )
			return __( 'Job Title', 'noo' );
		return $text;
	}
	add_filter( 'enter_title_here', 'jm_admin_job_edit_title_placeholder', 10, 2 );
endif;

if( !function_exists( 'jm_extend_job_status' ) ) :
	function jm_extend_job_status(){
		global $post, $post_type;
		if($post_type === 'noo_job'){
			$html = $selected_label = '';
			foreach ((array) jm_get_job_status() as $status=>$label){
				$seleced = selected($post->post_status,esc_attr($status),false);
				if($seleced)
					$selected_label = $label;
				$html .= "<option ".$seleced." value='".esc_attr($status)."'>".$label."</option>";
			}  
			?>
			<script type="text/javascript">
				jQuery( document ).ready( function($) {
					<?php if ( ! empty( $selected_label ) ) : ?>
						jQuery( '#post-status-display' ).html( '<?php echo esc_js( $selected_label ); ?>' );
					<?php endif; ?>
					var select = jQuery( '#post-status-select' ).find( 'select' );
					jQuery( select ).html( "<?php echo ($html); ?>" );
				} );
			</script>
			<?php
		}
	}

	foreach ( array( 'post', 'post-new' ) as $hook ) {
		add_action( "admin_footer-{$hook}.php", 'jm_extend_job_status' );
	}
endif;

if( !function_exists( 'jm_job_meta_boxes' ) ) :
	function jm_job_meta_boxes() {
		$helper = new NOO_Meta_Boxes_Helper( '', array( 'page' => 'noo_job' ) );

		$args = array(
			'post_type'			  => 'noo_company',
			'post_status'         => 'publish',
			'posts_per_page'	  => -1,
			'orderby'			  => 'title',
			'order'				  => 'ASC',
			'suppress_filters'	  => false
		);
		
		$companies = get_posts( $args );
		$company_option_arr = array( array( 'value' => '', 'label' => '' ) );
		foreach ($companies as $company) {
			if( !empty( $company->post_title ) ) {
				$company_option_arr[] = array(
					'value' => $company->ID,
					'label' => $company->post_title
				);
			}
		}

		$meta_box = array( 
			'id' => "job_settings", 
			'title' => __( 'Job Settings', 'noo' ), 
			'page' => 'noo_job', 
			'context' => 'normal', 
			'priority' => 'high', 
			'fields' => array( 
				array( 'id' => '_cover_image', 'label' => __( 'Cover Image', 'noo' ), 'type' => 'image' ), 
				array( 
					'id' => '_application_email', 
					'label' => __( 'Notification Email', 'noo' ), 
					'type' => 'text',
					'desc'=>__( 'Email to receive application notification. Leave it blank to use Employer\'s profile email.', 'noo' )
				),
				array( 'id' => 'author', 'label' => __( 'Posted by ( Employer - Company )', 'noo' ), 'type' => 'author','callback' => 'jm_meta_box_field_job_author' ),
				array(
					'id' => '_company_id',
					'label' => __( 'Company', 'noo' ),
					'type' => 'select',
					'desc'=>__( 'Use this option when you want to assign job to company without creating an employer.', 'noo' ),
					'options' => $company_option_arr
				), 
				// array(
				// 	'id' => '_closing',
				// 	'label' => __( 'Job Closing Date', 'noo' ),
				// 	'type' => 'datepicker',
				// ),
				array(
					'id' => '_expires',
					'label' => __( 'Job\'s Expiration Date', 'noo' ),
					'type' => 'datepicker',
				),
				array(
					'id' => '_canseled',
					'label' => __( 'Почему не опубликовали: ' ),
					'type' => 'select',
					'options' => array(
						array('value' => '',
						'label' => ''),
						array('value' => 'Ваша вакансия отклонена, вам нужно связаться с <b><a href="https://vk.com/im?sel=-36111404">администратором</a></b>',
						'label' => 'Обратитесь к администратору'),
						array('value' => 'Причина отклонения:<br> Не информативное описание вакансии.<p>Добавьте в описание вакансии:<br>- Адрес организации.<br>- С какой продукцией/услугой работаете.<br>- Чем конкретно придётся заниматься сотруднику.<br>',
						'label' => 'Не информативное описание'),
						array('value' => 'Причина отклонения:<br></br>Вы нарушели <a href="https://vk.com/write91326990"><i>правила</i></a> размещения.<br></br>Разместите другую вакансию, нажав <i class="fa fa-pencil"></i> или обратиться к <b><a href="https://vk.com/write91326990">администратору</a></b> для возврата денежных средств, указав название вакансии.<br>',
						'label' => 'Нарушение тип вакансии'),
						array('value' => 'Вакансия отклонена по причине:  Не информативное описание организации.<br> Что делать: Сделайте более подробное описание организации и обновите вакансию, нажав <i class="fa fa-pencil"></i> ',
                        'label' => 'Добавьте описание организации')
						) 
				),
			)
		);

		$custom_apply_link = jm_get_setting('noo_job_linkedin', 'custom_apply_link' );
		if( !empty( $custom_apply_link ) ) {
			$meta_box['fields'][] = array(
				'id' => '_custom_application_url', 
				'label' => __( 'Custom Application link', 'noo' ), 
				'type' => 'text',
				'desc'=>__('Job seekers will be redirected to this URL when they want to apply for this job.','noo')
			);
		}
		
		$helper->add_meta_box($meta_box);

		$fields = jm_get_job_custom_fields();
		if($fields){
			foreach ($fields as $field){
				if( !isset( $field['name'] ) || empty( $field['name'] ) ) continue;
				$field['type'] = !isset( $field['type'] ) || empty( $field['type'] ) ? 'text' : $field['type'];
				if( isset($field['is_default']) ) {
					if( isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes') )
						continue;
					if( isset( $field['is_tax'] ) )
						continue;
					$id = $field['name'];
				} else {
					$id = jm_job_custom_fields_name($field['name']);
				}

				$type = $field['type'];
				if( $field['type'] == 'multiple_select' ) {
					$type = 'select';
					$field['multiple'] = true;
				}

				if( $field['type'] == 'number' ) {
					$type = 'text';
				}

				if( in_array( $field['type'], array( 'multiple_select', 'select', 'checkbox', 'radio', '') ) ) {
					$field['options'] = array();
					$field_value = noo_convert_custom_field_setting_value( $field );
					foreach ($field_value as $key => $label) {
						$field['options'][] = array(
							'label' => $label,
							'value' => $key
							);
					}

					if( $field['type'] == 'checkbox' ) {
						$type = 'multiple_checkbox';
					}
				}

				$new_field = array(
					'label' => isset( $field['label_translated'] ) ? $field['label_translated'] : @$field['label'] ,
					'id' => $id,
					'type' => $type,
					'options' => isset( $field['options'] ) ? $field['options'] : '',
					'std' => isset( $field['std'] ) ? $field['std'] : '',
				);

				if( isset( $field['multiple'] ) && $field['multiple'] ) {
					$new_field['multiple'] = true;
				}

				$meta_box['fields'][] = $new_field;
			}
		}

		$helper->add_meta_box($meta_box);
	}

	add_action( 'add_meta_boxes', 'jm_job_meta_boxes', 30 );

endif;

if( !function_exists( 'jm_meta_box_field_job_author' ) ) :
	function jm_meta_box_field_job_author( $post, $id, $type, $meta, $std, $field){

		// $meta = !empty($meta) ? $meta : $std;
		$user_list = get_users( array( 'role' => Noo_Member::EMPLOYER_ROLE ) );
		$admin_list = get_users( array( 'role' => 'administrator' ) );
		$user_list = array_merge($admin_list, $user_list);

		echo'<select name="post_author_override" id="post_author_override" >';
		echo'	<option value="" '. selected( $post->post_author, '', true ) . '>' . __('- Select an Employer - ', 'noo') . '</option>';
		foreach ( $user_list as $user ) {
			$company_id = jm_get_employer_company($user->ID);
			echo'<option value="' . $user->ID . '"';
			selected( $post->post_author, $user->ID, true );
			echo '>' . $user->display_name;
			if( !empty($company_id) ) {
				$company_name = get_the_title( $company_id );
				echo ( !empty($company_name) ? ' - ' . $company_name : '' );
			}
			echo '</option>';
		}
		echo '</select>';
	}
endif;

if( !function_exists( 'jm_job_save_meta_box' ) ) :
	function jm_job_save_meta_box( $post_id ) {
		$meta_box = $_POST['noo_meta_boxes'];
		if( !isset($meta_box['_company_id']) || empty($meta_box['_company_id']) ) {
			$employer_id = get_post_field( 'post_author', $post_id );
			$company_id = jm_get_employer_company( $employer_id );

			if( $company_id ) {
				update_post_meta( $post_id, '_company_id', $company_id );
			}
		}
	}
	add_action( 'noo_save_meta_box', 'jm_job_save_meta_box' );
endif;
