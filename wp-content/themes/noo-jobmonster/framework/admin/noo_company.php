<?php
if(!class_exists('Noo_Company')):
	if( !class_exists('Noo_CPT') ) {
		require_once dirname(__FILE__) . '/noo_cpt.php';
	}
	class Noo_Company extends Noo_CPT {

		static $instance = false;
		static $employers = array();
		static $companies = array();

		public function __construct(){

			$this->post_type = 'noo_company';
			$this->slug = 'companies';
			$this->prefix = 'company';
			$this->option_key = 'noo_company';

			add_action( 'init', array( $this, 'register_post_type' ),0 );
			add_filter( 'template_include', array( $this, 'template_loader' ) );
			add_action( 'pre_get_posts', array ($this, 'pre_get_posts'), 1 );
			add_filter( 'posts_search', array ($this, 'search_by_title_only' ), 500, 2 );
			add_shortcode( 'noo_companies', array ($this, 'noo_companies_shortcode'), 2 );
			add_shortcode( 'noo_company_feature', array ($this, 'noo_company_feature_shortcode'), 3 );
			add_filter( 'redirect_canonical', array ($this, 'custom_disable_redirect_canonical' ) );

			if( is_admin() ) {
				add_action('admin_init', array($this,'admin_init'));
				$this->_company_featured();

				add_filter( 'display_post_states', array( $this, 'admin_page_state' ), 10, 2 );
				add_action( 'add_meta_boxes', array( $this, 'companies_page_notice' ), 10, 2 );
				add_action( 'add_meta_boxes', array($this, 'remove_meta_boxes' ), 20 );
				add_action( 'add_meta_boxes', array($this, 'add_meta_boxes' ), 30 );
				add_filter( 'enter_title_here', array ($this,'custom_enter_title') );

				add_filter('manage_edit-' . $this->post_type . '_columns', array($this, 'manage_edit_columns'));
				add_action('manage_posts_custom_column', array($this, 'manage_posts_custom_column'));


				add_filter('noo_job_settings_tabs_array', array($this,'add_seting_company_tab'));
				add_action('noo_job_setting_company', array($this,'setting_page'));

				add_filter('wp_insert_post', array($this,'default_company_data'), 10, 3);

				// add_filter('months_dropdown_results', '__return_empty_array');
				// add_action( 'restrict_manage_posts', array($this, 'restrict_manage_posts') );
				// add_filter( 'parse_query', array($this, 'posts_filter') );
			}
		}

		public static function get_setting($id = null ,$default = null){
			global $noo_company_setting;
			if(!isset($noo_company_setting) || empty($noo_company_setting)){
				$noo_company_setting = get_option('noo_company');
			}
			if (isset($noo_company_setting[$id])) {
				return $noo_company_setting[$id];
			}
			return $default;
		}
	
		public function admin_init(){
			register_setting('noo_company','noo_company');
		}

		public function template_loader($template){
			if(is_post_type_archive( $this->post_type ) ){
				$template       = locate_template( "archive-{$this->post_type}.php" );
			}
			return $template;
		}

		public function register_post_type() {
			// Sample register post type
			$archive_slug = self::get_setting('archive_slug', 'companies');
			$archive_slug = empty( $archive_slug ) ? 'companies' : $archive_slug;

			register_post_type( 
				$this->post_type,
				array( 
					'labels' => array( 
						'name' => __( 'Companies', 'noo' ), 
						'singular_name' => __( 'Company', 'noo' ), 
						'menu_name' => __( 'Companies', 'noo' ),
						'all_items' => __( 'Companies', 'noo' ),
						'add_new' => __( 'Add New', 'noo' ),
						'add_new_item' => __( 'Add Company', 'noo' ),
						'edit' => __( 'Edit', 'noo' ), 
						'edit_item' => __( 'Edit Company', 'noo' ), 
						'new_item' => __( 'New Company', 'noo' ), 
						'view' => __( 'View', 'noo' ), 
						'view_item' => __( 'View Company', 'noo' ), 
						'search_items' => __( 'Search Company', 'noo' ), 
						'not_found' => __( 'No Companies found', 'noo' ), 
						'not_found_in_trash' => __( 'No Companies found in Trash', 'noo' )
					), 
					'public' => true,
					'has_archive' => true,
					'show_in_menu' => 'edit.php?post_type=noo_job',
					'rewrite' => array( 'slug' => $archive_slug, 'with_front' => false ), 
					'supports' => array(
						'title',
						'editor'
					),
				)
			);
		}

		public function admin_page_state( $states = array(), $post = null ) {
			if( !empty( $post ) && is_object( $post ) ) {
				$archive_slug = self::get_setting('archive_slug', 'companies');
				if( !empty( $archive_slug ) && $archive_slug == $post->post_name ) {
					$states['companies_page'] = __('Companies Page', 'noo');
				}
			}

			return $states;
		}

		public function companies_page_notice( $post_type = '', $post = null ) {
			if( !empty( $post ) && is_object( $post ) ) {
				$archive_slug = self::get_setting('archive_slug', 'companies');
				if ( !empty( $archive_slug ) && $archive_slug == $post->post_name && empty( $post->post_content ) ) {
					add_action( 'edit_form_after_title', array( $this, '_companies_page_notice' ) );
					remove_post_type_support( $post_type, 'editor' );
				}
			}
		}

		public function _companies_page_notice() {
			echo '<div class="notice notice-warning inline"><p>' . __( 'You are currently editing the page that shows all your companies.', 'noo' ) . '</p></div>';
		}

		public function remove_meta_boxes() {
			// Remove slug and revolution slider
			remove_meta_box( 'mymetabox_revslider_0', $this->post_type, 'normal' );
		}

		public function add_meta_boxes() {
			// Declare helper object
			$prefix = '';
			$helper = new NOO_Meta_Boxes_Helper( $prefix, array( 'page' => $this->post_type ) );

			// General Info
			$meta_box = array(
				'id'           => '_general_info',
				'title'        => __( 'Company Infomation', 'noo' ),
				'context'      => 'normal',
				'priority'     => 'core',
				'description'  => '',
				'fields'       => array(
					array( 'id' => '_logo', 'label' => __( 'Company Logo', 'noo' ), 'type' => 'image' ), 
					array( 'id' => '_cover_image', 'label' => __( 'Cover Image', 'noo' ), 'type' => 'image' ), 
					array( 
						'id' => '_website', 
						'label' => __( 'Company Website', 'noo' ), 
						'type' => 'text' ), 
					array( 
						'id' => '_facebook', 
						'label' => __( 'Facebook URL', 'noo' ), 
						'type' => 'text' ), 
					array( 
						'id' => '_twitter', 
						'label' => __( 'Twitter URL', 'noo' ), 
						'type' => 'text' ), 
					array( 
						'id' => '_googleplus', 
						'label' => __( 'Google + URL', 'noo' ), 
						'type' => 'text' ), 
					array( 
						'id' => '_linkedin', 
						'label' => __( 'Linkedin URL', 'noo' ), 
						'type' => 'text' ), 
					array( 
						'id' => '_instagram', 
						'label' => __( 'Instagram URL', 'noo' ), 
						'type' => 'text' )
				),
			);

			$helper->add_meta_box($meta_box);
		}

		public function custom_enter_title( $input ) {
			global $post_type;

			if ( $this->post_type == $post_type )
				return __( 'Company Name', 'noo' );

			return $input;
		}

		public function manage_edit_columns($columns) {
			
			if ( ! is_array( $columns ) ) $columns = array();

			$before = array_slice($columns, 0, 2);
			$after = array_slice($columns, 2);
			
			$new_columns = array(
				'company_featured' => '<span class="tips" data-tip="' . __( "Is Company Featured?", 'noo' ) . '">' . __( "Featured?", 'noo' ) . '</span>',
			);

			$columns = array_merge($before, $new_columns, $after);
			return $columns;
		}

		public function manage_posts_custom_column($column) {
			global $post, $wpdb;
			switch ( $column ) {
				case "company_featured" :
					$featured = noo_get_post_meta($post->ID,'_company_featured');
					// Update old data
					if( empty( $featured ) ) update_post_meta( $post->ID, '_company_featured', 'no' );

					$url = wp_nonce_url( admin_url( 'admin-ajax.php?action=noo_company_feature&company_id=' . $post->ID ), 'noo-company-feature' );
					echo '<a href="' . esc_url( $url ) . '" title="'. __( 'Toggle featured', 'noo' ) . '">';
					if ( 'yes' === $featured ) {
						echo '<span class="noo-company-feature" title="'.esc_attr__('Yes','noo').'"><i class="dashicons dashicons-star-filled "></i></span>';
					} else {
						echo '<span class="noo-company-feature not-featured"  title="'.esc_attr__('No','noo').'"><i class="dashicons dashicons-star-empty"></i></span>';
					}
					echo '</a>';
				
				break;

			}
		}
	
		public function add_seting_company_tab($tabs){
			$temp1 = array_slice($tabs, 0, 3);
			$temp2 = array_slice($tabs, 3);

			$company_tab = array( 'company' => __('Company','noo') );
			return array_merge($temp1, $company_tab, $temp2);
		}

		public function setting_page(){
			if(isset($_GET['settings-updated']) && $_GET['settings-updated']) {
				flush_rewrite_rules();
			}
			?>
				<?php settings_fields('noo_company'); ?>
				<h3><?php echo __('Company Options','noo')?></h3>
				<table class="form-table" cellspacing="0">
					<tbody>
						<tr>
							<th>
								<?php esc_html_e('Companies Archive base (slug)','noo')?>
							</th>
							<td>
								<?php $archive_slug = self::get_setting('archive_slug', 'companies'); ?>
								<input type="text" name="noo_company[archive_slug]" value="<?php echo ($archive_slug ? $archive_slug : 'companies') ?>">
							</td>
						</tr>
						<tr>
							<th>
								<?php esc_html_e('Show Company with no Jobs','noo')?>
							</th>
							<td>
								<?php $show_no_jobs = self::get_setting('show_no_jobs',1); ?>
								<input type="hidden" name="noo_company[show_no_jobs]" value="">
								<input type="checkbox" <?php checked( $show_no_jobs, '1' ); ?> name="noo_company[show_no_jobs]" value="1">
							</td>
						</tr>
						<tr>
							<th>
								<?php esc_html_e('Enable Cover Image','noo')?>
							</th>
							<td>
								<?php $cover_image = self::get_setting('cover_image', 'yes'); ?>
								<input type="hidden" name="noo_company[cover_image]" value="">
								<input type="checkbox" <?php checked( $cover_image, 'yes' ); ?> name="noo_company[cover_image]" value="yes">
								<p><small><?php echo __('Allow Employers to change cover image. This image will be displayed as header background on a company\'s page and as a default one of single job pages.','noo') ?></small></p>
							</td>
						</tr>
						<?php do_action( 'noo_setting_company_fields' ); ?>
					</tbody>
				</table>
			<?php 
		}

		public function default_company_data($post_ID = 0, $post = null, $update = false) {

			if( !$update && !empty( $post_ID ) && $post->post_type == 'noo_company' ) {
				update_post_meta( $post_ID, '_company_featured', 'no' );
			}
		}

		public function pre_get_posts( $query ) {
			if ( is_admin() || $query->is_singular ) {
				return;
			}

			//if is querying noo_company
			if(isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'noo_company' ) {

				$query->query_vars['posts_per_page'] = -1;
				$query->query_vars['orderby'] = 'title';
				$query->query_vars['order'] = 'ASC';
			}

			return;
		}

		public function search_by_title_only( $search, &$wp_query ) {
			//if is querying noo_company
			if(isset($_GET['post_type']) && $_GET['post_type'] == 'noo_company' ) {
				if ( ! empty( $search ) && ! empty( $wp_query->query_vars['search_terms'] ) ) {
					global $wpdb;

					$q = $wp_query->query_vars;
					$n = ! empty( $q['exact'] ) ? '' : '%';

					$search = array();

					foreach ( ( array ) $q['search_terms'] as $term ) {
						$search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $term ) . $n );
					}

					$search = ' AND ' . implode( ' AND ', $search );
				}
			}

			return $search;
		}

		public function noo_companies_shortcode($atts, $content = null){
			extract(shortcode_atts(array(
				'title' => __( 'Companies', 'noo' )
			), $atts));
			$args = array(
				'post_type'			  => 'noo_company',
				'post_status'         => 'publish',
				'posts_per_page'	  => -1,
				'orderby'			  => 'title',
				'order'				  => 'ASC',
			);
			
			$r = new WP_Query($args);
			ob_start();
			self::loop_display(array('query'=>$r, 'title'=>$title));
			$output = ob_get_clean();
			return $output;
		}

		public function noo_company_feature_shortcode($atts, $content = null){
			extract(shortcode_atts(array(
				'title'            => __( 'Featured Employer', 'noo' ),
				'posts_per_page'   => -1,
				'featured_content' => '',
				'autoplay'         => 'false',
				'slider_speed'     => '800',
			), $atts));
			wp_enqueue_script( 'vendor-carouFredSel' );
			$args = array(
				'post_type'			  => 'noo_company',
				'post_status'         => 'publish',
				'posts_per_page'	  => $posts_per_page,
				'orderby'			  => 'title',
				'order'				  => 'DESC',
			);

			$args['meta_query'][] = array(
				'key'   => '_company_featured',
				'value' => 'yes'
			);
			
			$r = new WP_Query($args);
			ob_start();
			self::loop_display(array(
				'query'            => $r, 
				'title'            => $title, 
				'style'            => 'slider', 
				'featured_content' => $content,
				'autoplay'         => $autoplay,
				'slider_speed'     => $slider_speed
			));
			$output = ob_get_clean();
			return $output;
		}

		public static function get_employer_id( $company_id = null ) {
			if( empty( $company_id ) ) return 0;

			if( isset( self::$employers[$company_id] ) ) {
				return self::$employers[$company_id];
			}

			$employers = get_users( array( 'meta_key' => 'employer_company', 'meta_value' => $company_id, 'fields' => 'id' ) );

			if( empty( $employers ) ) {
				self::$employers[$company_id] = 0;
			} else {
				self::$employers[$company_id] = $employers[0];
			}

			return self::$employers[$company_id];
		}

		public static function count_jobs( $company_id = null ) {
			if( empty( $company_id ) ) return 0;
			global $wpdb;

			$employer = self::get_employer_id( $company_id );

			if( !defined( 'ICL_SITEPRESS_VERSION' ) ) {
				$company_jobs = $wpdb->get_var( "SELECT COUNT( DISTINCT p.ID ) FROM {$wpdb->posts} AS p 
					LEFT JOIN {$wpdb->postmeta} AS pm
					ON p.ID = pm.post_id AND pm.meta_key = '_company_id'
					WHERE p.post_type = 'noo_job' AND p.post_status = 'publish'
						AND ( pm.meta_value = '{$company_id}'
							OR (
								p.post_author = {$employer}
								AND ( pm.meta_value IS NULL OR pm.meta_value = '' )
							)
						)");
			} else {
				$company_jobs = $wpdb->get_var( "SELECT COUNT( DISTINCT p.ID ) FROM {$wpdb->posts} AS p 
					LEFT JOIN {$wpdb->postmeta} AS pm
					ON p.ID = pm.post_id AND pm.meta_key = '_company_id'
					LEFT JOIN {$wpdb->prefix}icl_translations AS wpml
					ON p.ID = wpml.element_id
					WHERE p.post_type = 'noo_job' AND p.post_status = 'publish'
						AND ( pm.meta_value = '{$company_id}'
							OR (
								p.post_author = {$employer}
								AND ( pm.meta_value IS NULL OR pm.meta_value = '' )
							)
						)
						AND wpml.language_code = '".ICL_LANGUAGE_CODE."'"
					);
			}

			return  absint( $company_jobs );
		}

		public static function get_company_jobs( $company_id = null, $exclude_job_ids = array(), $number_of_jobs = -1, $status = 'publish' ) {
			if( empty( $company_id ) ) return array();
			global $wpdb;

			$employer = self::get_employer_id( $company_id );

			if( !defined( 'ICL_SITEPRESS_VERSION' ) ) {
				$query = "SELECT DISTINCT p.ID FROM {$wpdb->posts} AS p 
					LEFT JOIN {$wpdb->postmeta} AS pm
					ON p.ID = pm.post_id AND pm.meta_key = '_company_id'
					WHERE p.post_type = 'noo_job' AND p.post_status = 'publish'
						AND ( pm.meta_value = '{$company_id}'
							OR (
								p.post_author = {$employer}
								AND ( pm.meta_value IS NULL OR pm.meta_value = '' )
							)
						)";
			} else {
				$query = "SELECT DISTINCT p.ID FROM {$wpdb->posts} AS p 
					LEFT JOIN {$wpdb->postmeta} AS pm
					ON p.ID = pm.post_id AND pm.meta_key = '_company_id'
					LEFT JOIN {$wpdb->prefix}icl_translations AS wpml
					ON p.ID = wpml.element_id
					WHERE p.post_type = 'noo_job' AND p.post_status = 'publish'
						AND ( pm.meta_value = '{$company_id}'
							OR (
								p.post_author = {$employer}
								AND ( pm.meta_value IS NULL OR pm.meta_value = '' )
							)
						)
						AND wpml.language_code = '".ICL_LANGUAGE_CODE."'";
			}

			if( !empty( $status ) && $status != 'all' ) {
				$query .= " AND p.post_status = '{$status}'";
			}

			if( !empty( $exclude_job_ids ) ) {
				$query .= " AND p.ID NOT IN ( " . implode(',', $exclude_job_ids ) . " )";
			}

			$query .= "  ORDER BY p.post_date DESC";

			if( $number_of_jobs > 0 ) {
				$query .= " LIMIT 0, {$number_of_jobs}";
			}

			return $company_jobs = $wpdb->get_col( $query );
		}

		public static function get_more_jobs( $company_id = null, $exclude_job_ids = array(), $number_of_jobs = 5 ) {
			self::get_company_jobs( $company_id, $exclude_job_ids, $number_of_jobs );
		}

		public function custom_disable_redirect_canonical( $redirect_url ){
			global $post;
			$ptype = get_post_type( $post );
			if ( $ptype == 'noo_company' ) $redirect_url = false;
			return $redirect_url;
		}

		public static function get_company_logo( $company_id = 0, $size = 'company-logo' ) {
			if( empty( $company_id ) ) return '';

			$size_key = is_array( $size ) ? implode( '_', $size ) : $size;
			if( !isset( self::$companies[$company_id] ) ) {
				self::$companies[$company_id] = array();
			}

			if( !isset( self::$companies[$company_id][$size_key] ) ) {
				$class			= apply_filters( 'noo_company_logo_class', '', $company_id );
				$thumbnail_id	= noo_get_post_meta($company_id, '_logo', '');
				$size			= is_numeric( $size ) ? array( $size, $size ) : $size;
				$company_logo	= wp_get_attachment_image($thumbnail_id, $size, false, array( 'class' => $class ) );
				if(empty($company_logo)){
					$img_size = '';
					if( is_array( $size ) ) {
						$size[1] = count( $size ) > 1 ? $size[1] : $size[0];
						$img_size = 'width="' . $size[0] . 'px" height="' . $size[1] . 'px"';
					}

					$company_logo = '<img src="'.NOO_ASSETS_URI.'/images/company-logo.png" ' . $img_size . ' class="' . $class . '">';	
				}

				self::$companies[$company_id][$size_key] = $company_logo;
			}

			return apply_filters( 'noo_company_logo', self::$companies[$company_id][$size_key], $company_id );
		}

		public static function loop_display( $args = '' ) {
			$defaults = array( 
				'query'        => '', 
				'title'        => '',
				'style'        => '',
				'content'      => '',
				'autoplay'     => 'false',
				'slider_speed' => '800'
			);
			$p = wp_parse_args( $args, $defaults );
			extract($p);
			global $wp_query;
			if(!empty($query))
				$wp_query = $query;

			// ===== Option slider
				$option = array(
					'autoplay'     => $autoplay,
					'slider_speed' => $slider_speed
				);

			ob_start();
	        include(locate_template("layouts/noo_company-loop.php"));
	        echo ob_get_clean();

			wp_reset_postdata();
			wp_reset_query();
		}
		public static function display_sidebar( $company_id = null, $show_more_job = false ){

			if(empty($company_id)) {
				return;
			}

			ob_start();
	        include(locate_template("layouts/noo_company-info.php"));
	        echo ob_get_clean();

			wp_reset_postdata();
			wp_reset_query();
		}
		protected function _company_featured(){
			if(isset($_GET['action']) && $_GET['action'] == 'noo_company_feature'){
				// echo 'ok'; die;
				if ( ! current_user_can( 'edit_posts' ) ) {
					wp_die( __( 'You do not have sufficient permissions to access this page.', 'noo' ), '', array( 'response' => 403 ) );
				}
		
				if ( ! check_admin_referer( 'noo-company-feature' ) ) {
					wp_die( __( 'You have taken too long. Please go back and retry.', 'noo' ), '', array( 'response' => 403 ) );
				}
		
				$post_id = ! empty( $_GET['company_id'] ) ? (int) $_GET['company_id'] : '';
		
				if ( ! $post_id || get_post_type( $post_id ) !== 'noo_company' ) {
					die;
				}
		
				$featured = noo_get_post_meta( $post_id, '_company_featured' );
		
				if ( 'yes' === $featured ) {
					update_post_meta( $post_id, '_company_featured', 'no' );
				} else {
					update_post_meta( $post_id, '_company_featured', 'yes' );
				}
		
		
				wp_safe_redirect( esc_url_raw( remove_query_arg( array( 'trashed', 'untrashed', 'deleted', 'ids' ), wp_get_referer() ) ) );
				die();
			}
		}
	}

	new Noo_Company();
endif;