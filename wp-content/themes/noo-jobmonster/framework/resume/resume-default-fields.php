<?php

if( !function_exists( 'jm_get_resume_default_fields' ) ) :
	function jm_get_resume_default_fields() {
		$default_fields = array(
			'_language' => array(
					'name' => '_language',
					'label' => __('Language', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'Your working language', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_highest_degree' => array(
					'name' => '_highest_degree',
					'label' => __('Highest Degree Level', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'eg. &quot;Bachelor Degree&quot;', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_experience_year' => array(
					'name' => '_experience_year',
					'label' => __('Total Years of Experience', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'eg. &quot;1&quot;, &quot;2&quot;', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_job_category' => array(
					'name' => '_job_category',
					'label' => __('Job Category', 'noo'),
					'type' => 'multiple_select',
					'allowed_type' => array(
						'select'			=> __('Select', 'noo'),
						'multiple_select'	=> __( 'Multiple Select', 'noo' ),
						'radio'				=> __( 'Radio', 'noo' ),
						'checkbox'			=> __( 'Checkbox', 'noo' )
					),
					'value' => '',
					'std' => '',
					'is_default' => true,
					'is_tax' => true,
					'required' => true
				),
			'_job_level' => array(
					'name' => '_job_level',
					'label' => __('Expected Job Level', 'noo'),
					'type' => 'text',
					'value' => '',
					'std' => __( 'eg. &quot;Junior&quot;, &quot;Senior&quot;', 'noo' ),
					'is_default' => true,
					'required' => false
				),
			'_job_location' => array(
					'name' => '_job_location',
					'label' => __('Job Location', 'noo'),
					'type' => 'multiple_select',
					'allowed_type' => array(
						'select'			=> __('Select', 'noo'),
						'multiple_select'	=> __( 'Multiple Select', 'noo' ),
						'radio'				=> __( 'Radio', 'noo' ),
						'checkbox'			=> __( 'Checkbox', 'noo' )
					),
					'value' => '',
					'std' => '',
					'is_default' => true,
					'is_tax' => true,
					'required' => false
				),
			);

		return apply_filters( 'jm_resume_default_fields', $default_fields );
	}
endif;


if( !function_exists( 'jm_resume_tax_field_params' ) ) :
	function jm_resume_tax_field_params( $args = array(), $resume_id = 0 )  {
		extract($args);

		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;

		if( in_array( $field['name'], array( '_job_category', '_job_location' ) ) ) {
			$field_id = $field['name'];

			$field_value = array();
			$term_id = str_replace('_job_', 'job_', $field_id);
			$terms = get_terms( $term_id, array( 'hide_empty' => 0 ) );
			foreach ($terms as $term) {
				$field_value[] = $term->term_id . '|' . $term->name;
			}
			$field['value'] = $field_value;
			$field['no_translate'] = true;

			if( !empty( $resume_id ) ) {
				$value = $resume_id ? noo_get_post_meta( $resume_id, $field_id, '' ) : $value;
				$value = noo_json_decode( $value );
			}

			if( empty( $field['type'] ) || $field['type'] == 'text' ) {
				$default_fields = jm_get_resume_default_fields();
				$field['type'] = $default_fields[$field['name']]['type'];
			}
		}

		return compact( 'field', 'field_id', 'value' );
	}
	
	add_filter( 'jm_resume_render_form_field_params', 'jm_resume_tax_field_params', 10, 3 );
	add_filter( 'jm_resume_render_search_field_params', 'jm_resume_tax_field_params' );
endif;