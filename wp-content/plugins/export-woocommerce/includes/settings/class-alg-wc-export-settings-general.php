<?php
/**
 * Export WooCommerce - General Section Settings
 *
 * @version 1.2.0
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_Export_Settings_General' ) ) :

class Alg_WC_Export_Settings_General extends Alg_WC_Export_Settings_Section {

	/**
	 * Constructor.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function __construct() {
		$this->id   = '';
		$this->desc = __( 'General', 'export-woocommerce' );
		parent::__construct();
	}

	/**
	 * add_settings.
	 *
	 * @version 1.2.0
	 * @since   1.0.0
	 * @todo    add link to tools Dashboard
	 */
	function add_settings( $settings ) {
		$settings = array(
			array(
				'title'     => __( 'Export Options', 'export-woocommerce' ),
				'type'      => 'title',
				'id'        => 'alg_wc_export_options',
			),
			array(
				'title'     => __( 'CSV Separator', 'export-woocommerce' ),
				'id'        => 'alg_export_csv_separator',
				'default'   => ',',
				'type'      => 'text',
			),
			array(
				'title'     => __( 'CSV Wrap', 'export-woocommerce' ),
				'id'        => 'alg_export_csv_wrap',
				'default'   => '',
				'type'      => 'text',
			),
			array(
				'title'     => __( 'UTF-8 BOM', 'export-woocommerce' ),
				'desc'      => __( 'Add', 'export-woocommerce' ),
				'desc_tip'  => __( 'Add UTF-8 BOM sequence', 'export-woocommerce' ),
				'id'        => 'alg_export_csv_add_utf_8_bom',
				'default'   => 'yes',
				'type'      => 'checkbox',
			),
			array(
				'title'     => __( 'User Capability', 'export-woocommerce' ),
				'desc_tip'  => __( 'Set required user capability for CSV and XML export. Leave blank if you want all users (including not logged) to be able to export the data (not recommended).', 'export-woocommerce' ),
				'desc'      => sprintf( __( 'Default: %s.', 'export-woocommerce' ), '<code>manage_options</code>' ),
				'id'        => 'alg_export_csv_xml_user_capability',
				'default'   => 'manage_options',
				'type'      => 'text',
			),
			array(
				'type'      => 'sectionend',
				'id'        => 'alg_wc_export_options',
			),
		);
		return $settings;
	}

}

endif;

return new Alg_WC_Export_Settings_General();
