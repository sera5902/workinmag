<?php 
$company_id = isset($_GET['company_id']) ? absint($_GET['company_id']) : 0;
$company = get_post($company_id);
$user_ID = get_current_user_id(); 
?>
<div class="company-profile-form">
	<div class="form-group  row">
		<label for="company_name" class="col-sm-3 control-label"><?php _e('Company Name','noo')?></label>
		<div class="col-sm-9">
	    	<input type="text" class="form-control" required id="company_name" value="<?php echo ($company_id ? $company->post_title  : '')?>"  name="company_name" placeholder="<?php echo esc_attr__('Enter your company name','noo')?>">
	    	<span class="error" style="display: none; color: red;">Введите полное название организации или магазина.</span>
	    </div>
	</div>
	
	<div class="form-group  row">
	    <label for="company_desc" class="col-sm-3 control-label"><?php _e('Company Description','noo')?></label>
	    <div class="col-sm-9">
	    	<textarea class="form-control form-control-editor ignore-valid jform-validate" id="company_desc"  name="company_desc" rows="8" placeholder="<?php echo esc_attr__('Enter your company description','noo')?>"><?php echo ($company_id ? $company->post_content : '') ?></textarea>
	    </div>
	</div>
	<div class="form-group  row">
		<label class="col-sm-3 control-label"><?php _e('Company Logo','noo')?></label>
		<div class="col-sm-9">
			<?php
				$company_logo = ($company_id ? noo_get_post_meta($company_id,'_logo') : '');
				noo_image_upload_form_field( 'company_logo', $company_logo );
			?>
		</div>
	</div>
	<?php if( Noo_Company::get_setting('cover_image', 'yes') == 'yes' ) : ?>
		<div class="form-group row">
			<label class="col-sm-3 control-label"><?php _e('Cover Image','noo')?></label>
			<div class="col-sm-9">
				<?php
					$company_cover_image = ($company_id ? noo_get_post_meta($company_id,'_cover_image') : '');
					noo_image_upload_form_field( 'cover_image', $company_cover_image );
				?>
			</div>
		</div>
	<?php endif; ?>
		
</div>

<script type="text/javascript">
	jQuery( document ).ready(function() {
		var Value = jQuery('#company_name').val();
		if ( Value.indexOf("ИП")!== -1 || Value.indexOf("ООО")!== -1 ) {
			jQuery('.company-profile-form .error').css('display' ,'inline-block');
			jQuery('#company_profile_form .btn-primary').css('pointer-events' ,'none');
		}
		else {
			jQuery('.company-profile-form .error').css('display' ,'none');
			jQuery('#company_profile_form .btn-primary').css('pointer-events' ,'auto');
		}
		jQuery('#company_name').keyup(function(){
		var Value = jQuery('#company_name').val();
		if ( (Value.indexOf("ООО")!== -1 || Value.indexOf("OOO")!== -1 || Value.indexOf("ооо")!== -1 || Value.indexOf("ООо")!== -1 || Value.indexOf("Ооо")!== -1 || Value.indexOf("оОО")!== -1 || Value.indexOf("ооО")!== -1) && jQuery(this).val().length == 3) {
			jQuery('.company-profile-form .error').css('display' ,'inline-block');
			jQuery('#company_profile_form .btn-primary').css('pointer-events' ,'none');

		} else if (  (Value.indexOf("ИП")!== -1 || Value.indexOf("Ип")!== -1 || Value.indexOf("иП")!== -1 || Value.indexOf("ип")!== -1) && jQuery(this).val().length == 2) {
			jQuery('.company-profile-form .error').css('display' ,'inline-block');
			jQuery('#company_profile_form .btn-primary').css('pointer-events' ,'none');

		}else {
			jQuery('.company-profile-form .error').css('display' ,'none');
			jQuery('#company_profile_form .btn-primary').css('pointer-events' ,'auto');
		}
		
		});
	});
</script>