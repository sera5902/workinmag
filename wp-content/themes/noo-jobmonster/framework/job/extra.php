<?php
if( !function_exists('jm_correct_job_status') ) :
	function jm_correct_job_status( $job_id = null, $job_status = 'pending' ) {
		if ( empty( $job_id ) ) {
			return;
		}
		$corrected_status = '';
		if( $job_status == 'pending' ) {
			$in_review = (bool) noo_get_post_meta( $job_id, '_in_review', '' );
			$waiting_payment = (bool) noo_get_post_meta( $job_id, '_waiting_payment', '' );
			$canseledet_why = noo_get_post_meta( $job_id, '_canseled' );
			if( !$in_review && !$waiting_payment && $canseledet_why == '') {
				$corrected_status = 'inactive';
			} elseif( $waiting_payment ) {
				delete_post_meta( $job_id, '_waiting_payment' );
				$corrected_status = 'pending_payment';
			}
		}

		// Correct for version 2.10.1 or below
		if( !empty( $corrected_status ) ) {
			wp_update_post(array(
				'ID'=>$job_id,
				'post_status' => $corrected_status,
				)
			);

			return $corrected_status;
		}

		return $job_status;
	}
endif;

if( !function_exists('jm_correct_application_attachment') ) :
	function jm_correct_application_attachment( $application_id = 0 ) {
		if( empty( $application_id ) ) return;
		$attachment = noo_get_post_meta( $application_id, '_attachment', '' );

		if( !empty( $attachment ) && is_numeric( $attachment ) ) {
			$resume = noo_get_post_meta( $application_id, '_resume', '' );
			if( empty( $resume ) ) {
				$maybe_resume = absint($attachment);
				// if( 'noo_resume' === get_post_type( $maybe_resume ) ) {
					update_post_meta( $application_id, '_attachment', '' );
					update_post_meta( $application_id, '_resume', $maybe_resume );
				// }
				return '';
			}
		}
		
		return $attachment;
	}
endif;

// add_action( 'upgrader_process_complete', 'jm_upgrate_theme_ver_300',10, 2);

function jm_upgrate_theme_ver_300( $upgrader_object, $options ) {
	$current_plugin_path_name = plugin_basename( __FILE__ );

	if ($options['action'] == 'update' && $options['type'] == 'theme' ){
		foreach($options['packages'] as $each_theme){
			if ($each_theme=='noo-jobmonster' ) {

				$theme_data = wp_get_theme( 'noo-jobmonster' );
				$theme_version = $theme_data->Version;
				if( version_compare( $theme_version, '3.0.0', '>=' ) ) {
					$jm_data_version = get_option( 'jm_data_version' );
					if ( $jm_data_version == '3.0.0' ) {
						return;
					}

					global $wpdb;

					$job_ids = $wpdb->get_col( "
						SELECT ID FROM {$wpdb->posts}
						WHERE post_status = 'noo_job'
						AND post_type = 'pending'" );

					if ( $job_ids ) {
						foreach ( $job_ids as $job_id ) {
							jm_correct_job_status( $job_id );
						}
					}

					update_option( 'jm_data_version', '3.0.0' );
				}
			}
		}
	}
}

function jm_correct_settings() {
	$member_apply = jm_get_application_setting( 'member_apply','');
	$disable_member_upload = jm_get_application_setting( 'disable_member_upload','');
	$application_attachment = jm_get_application_setting( 'application_attachment', '' );
	if( empty( $application_attachment ) && $member_apply == 'yes' && $disable_member_upload ==  'yes' ) {
		$options = get_option('noo_job_linkedin');
		$options['application_attachment'] = 'disabled';
		update_option( 'noo_job_linkedin', $options );
	}
}
