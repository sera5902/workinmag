<?php do_action('noo_member_manage_plan_before'); ?>
<?php
	$package = array();
	$package_page_id = '';
	if( Noo_Member::is_employer() ) {
		$package = jm_get_job_posting_info();
		$package_page_id = Noo_Job_Package::get_setting( 'package_page_id' );
	} elseif( Noo_Member::is_candidate() ) {
		$package = jm_get_resume_posting_info();
		$package_page_id = Noo_Resume_Package::get_setting( 'resume_package_page_id' );
	}

	$job_added = jm_get_job_posting_added();
	$feature_job_remain = jm_get_feature_job_remain();
?>
<div class="member-plan">
	<?php if(empty($package)) : ?>
		<p class="no-plan-package text-center"><?php _e('You have no active packages','noo') ?></p>
		<div class="member-plan-choose">
			<a class="btn btn-lg btn-primary" href="<?php echo esc_url(get_permalink( $package_page_id ))?>"><?php _e('Choose a Package','noo')?></a>
		</div>
	<?php else : ?>
		<div class="row">
			<?php if( isset( $package['product_id'] ) && !empty( $package['product_id'] ) ) :?>
				<div class="col-xs-6"><strong><?php _e('Plan','noo')?></strong></div>
				<div class="col-xs-6"><?php echo esc_html(get_the_title(absint($package['product_id']))) ?></div>
			<?php endif;?>
			<?php if( Noo_Member::is_employer() ) : ?>
				<div class="col-xs-6"><strong><?php _e('Job Limit','noo')?></strong></div>
				<div class="col-xs-6"><?php echo sprintf(__('%s job(s)','noo'), $package['job_limit'] == 99999999 ? __('Unlimited', 'noo') : $package['job_limit'] ) ?></div>
				<div class="col-xs-6"><strong><?php _e('Job Added','noo')?></strong></div>
				<div class="col-xs-6"><?php echo sprintf(__('%s job(s)','noo'), jm_get_job_posting_added()); ?></div>
				<div class="col-xs-6"><strong><?php _e('Job Duration','noo')?></strong></div>
				<div class="col-xs-6"><?php echo esc_html(sprintf(__('%s day(s)','noo'),$package['job_duration'])) ?></div>
				<?php if( isset( $package['job_featured'] ) && !empty( $package['job_featured'] ) ) : ?>
					<div class="col-xs-6"><strong><?php _e('Featured Job limit','noo')?></strong></div>
					<div class="col-xs-6"><?php echo sprintf(__('%s job(s)','noo'), $package['job_featured']); ?>
						<?php if( $feature_job_remain < $package['job_featured'] ) echo '&nbsp;' . sprintf( __('( %d remain )', 'noo'), $feature_job_remain ); ?>
					</div>
				<?php endif; ?>
			<?php else : ?>
				<div class="col-xs-6"><strong><?php _e('Resume Limit','noo')?></strong></div>
				<div class="col-xs-6"><?php echo sprintf(__('%s resume(s)','noo'), $package['resume_limit'] == 99999999 ? __('Unlimited', 'noo') : $package['resume_limit'] ) ?></div>
				<div class="col-xs-6"><strong><?php _e('Resume Added','noo')?></strong></div>
				<div class="col-xs-6"><?php echo sprintf(__('%s resume(s)','noo'), jm_get_resume_posting_added()); ?></div>
				<?php if( isset( $package['resume_featured'] ) && !empty( $package['resume_featured'] ) ) : ?>
					<div class="col-xs-6"><strong><?php _e('Featured Resume limit','noo')?></strong></div>
					<div class="col-xs-6"><?php echo esc_html($package['resume_featured']) ?></div>
				<?php endif; ?>
			<?php endif; ?>
			<?php do_action('jm_manage_plan_features_list', $package ); ?>
			<?php if( isset( $package['created'] ) && !empty( $package['created'] ) ):?>
				<div class="col-xs-6"><strong><?php _e('Date Activated','noo')?></strong></div>
				<div class="col-xs-6"><?php echo mysql2date(get_option('date_format'), $package['created']) ?></div>
			<?php elseif( isset( $package['counter_reset'] ) && !empty( $package['counter_reset'] ) ) :?>
				<div class="col-xs-12 text-center"><?php echo sprintf( __('Your counter will be reset every %d month','noo'), absint( $package['counter_reset'] ) );?></div>
			<?php endif;?>
		</div>
		<?php if(jm_is_woo_job_posting()) : ?>
			<div class="member-plan-choose">
				<?php if(jm_is_woo_job_posting()) : ?>
					<p class="woo-order-history-link"><?php _e('Or', 'noo'); ?>&nbsp;<a href="<?php echo get_permalink(woocommerce_get_page_id( 'myaccount' ))?>"><?php _e('see your order history', 'noo'); ?></a></p>
				<?php endif; ?>
			</div>
		<?php endif;?>
	<?php endif; ?>
</div>