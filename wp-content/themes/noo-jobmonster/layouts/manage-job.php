<?php
$r = jm_user_job_query();
$r2 = jm_user_job_query_only_canseled();
$package_data = jm_get_job_posting_info();
$remain_featured_job = jm_get_feature_job_remain();
ob_start();
do_action('noo_member_manage_job_before');
$bulk_actions = (array) apply_filters('noo_member_manage_job_bulk_actions', array(
	'publish'=>__('Publish','noo'),
	'unpublish'=>__('Unpublish','noo'),
	'delete'=>__('Delete','noo')
));
$job_need_approve = jm_get_job_setting( 'job_approve','' ) == 'yes';
?>
<div class="member-manage">
	<?php if($r->have_posts()):?>
		<div class="approve_parent">
		<div class="approvedet_jobs">
		<h3><?php echo sprintf(__("You've posted %s jobs",'noo'),$r->found_posts)?></h3>
		<em><strong><?php _e('Note:','noo')?></strong> <?php _e('Expired listings will be removed from public view.','noo')?></em><br/>
		<em><?php echo sprintf(__('You can set %d more job(s) to be featured. Featured jobs cannot be reverted.','noo'), $remain_featured_job);?></em>
		</div>
		<?php if($r2->have_posts()): ?>
		<div class="noapprove_jobs">
			<?php while ($r->have_posts()): $r->the_post();
			?>
			
			<div class="item__noapprove">
			    
			   <span class="pull-left"><?php if (noo_get_post_meta( $post->ID, '_canseled' ) !== '') the_title()?></span>
				<span class="pull-right"><?php if (noo_get_post_meta( $post->ID, '_canseled' ) !== '') echo noo_get_post_meta( $post->ID, '_canseled' )?></span>
			</div>

		<?php endwhile?>
		</div>
		<?php endif?>

		</div>
		<form method="post">
			<div class="member-manage-toolbar top-toolbar clearfix">
				<div class="bulk-actions clearfix">
					
					
					
				</div>
			</div>
			<div style="display: none">
			<?php wp_nonce_field('job-manage-action')?>
			</div>
			<div class="member-manage-table">
				<table class="table">
					<thead>
						<tr>
							<th class="check-column"><div class="form-control-flat"><label class="checkbox"><input type="checkbox"><i></i></label></div></th>
							<th><?php _e('Title','noo')?></th>
							<th class="hidden-xs"><?php _e('Featured?', 'noo'); ?></th>
							
							<th class="hidden-xs"><?php _e('Closing','noo')?></th>
							<th class="text-center"><?php _e('Apps','noo')?></th>
							<th class="text-center"><?php _e('Status','noo')?></th>
							<th class="text-center"><?php _e('Action','noo')?></th>
						</tr>
					</thead>
					<tbody>
						<?php while ($r->have_posts()): $r->the_post();global $post;
							$status = $status_class = jm_correct_job_status( $post->ID, $post->post_status );
							$statuses = jm_get_job_status();
							$status_text = '';
							if ( isset( $statuses[ $status ] ) ) {
								$status_text = $statuses[ $status ];
							} else {
								$status_text = __( 'Inactive', 'noo' );
								$status_class = 'inactive';
							}
						?>
							<tr>
								<td class="check-column"><div class="form-control-flat"><label class="checkbox"><input type="checkbox" name="ids[]" value="<?php the_ID()?>"><i></i></label></div></td>
								<td>
									<?php if( $status == 'pending' || $status == 'pending_payment' ) : ?>
										<a href="<?php echo esc_url(add_query_arg( 'job_id', get_the_ID(), Noo_Member::get_endpoint_url('preview-job') )); ?>"><strong><?php the_title()?></strong></a>
									<?php else : ?>
										<a href="<?php the_permalink()?>"><strong><?php the_title()?></strong></a>
									<?php endif; ?>
								</td>
								<td class="hidden-xs text-center">
									<?php 
									$featured = noo_get_post_meta($post->ID,'_featured');
									if( empty( $featured ) ) {
										// Update old data
										update_post_meta( $post->ID, '_featured', 'no' );
									}
									if ( 'yes' === $featured ) :
										echo '<span class="noo-job-feature" data-toggle="tooltip" title="'.esc_attr__('Featured','noo').'"><i class="fa fa-star"></i></span>';
									elseif( jm_can_set_feature_job() ) :
									?>
										<a href="<?php echo wp_nonce_url( add_query_arg(array('action'=>'featured','job_id'=>get_the_ID())), 'job-manage-action' )?>">
											<span class="noo-job-feature not-featured" data-toggle="tooltip"  title="<?php _e('Set Featured','noo'); ?>"><i class="fa fa-star-o"></i></span>
										</a>
									<?php else : ?>
										<span class="noo-job-feature not-featured" title="<?php _e('Set Featured','noo'); ?>"><i class="fa fa-star-o"></i></span>
									<?php endif; ?>
								</td>
								<td class="job-manage-expires">
									<?php 
										$closing = noo_get_post_meta( $post->ID, '_closing' );
										$closing = !is_numeric( $closing ) ? strtotime( $closing ) : $closing;
										$closing = !empty( $closing ) ? date_i18n( get_option('date_format'), $closing ) : '';
										if( !empty( $closing ) ) :
									?>
										<span>От: <em><?php the_date(); ?></em></span><br>
										<span>До: <em><?php echo $closing; ?></em></span>
									<?php else : ?>
										<span class="text-center"><?php echo __('Equal to expired date', 'noo'); ?></span>
									<?php endif; ?>
								</td>
								<td class="job-manage-app text-center">
									<span>
									<?php 
									$applications = get_posts(array(
										'post_type' => 'noo_application',
										'posts_per_page'=>-1,
										'post_parent'=>$post->ID,
										'post_status'=>array('publish','pending','rejected'),
										'suppress_filters' => false
									));
									echo absint(count($applications));
									?>
									</span>
								</td>
								<td class="text-center">
									<span class="jm-status jm-status-<?php echo esc_attr($status_class) ?>">
									<?php echo esc_html($status_text)?>
									</span>
								</td>
								<td class="member-manage-actions text-center">
									<?php if(Noo_Member::can_change_job_state( $post->ID, get_current_user_id() )):?>
										<?php if($status == 'publish'):?>
											<a href="<?php echo wp_nonce_url( add_query_arg(array('action'=>'unpublish','job_id'=>get_the_ID())), 'job-manage-action' );?>" class="member-manage-action"  data-toggle="tooltip" title="<?php esc_attr_e('Unpublish Job','noo')?>"><i class="fa fa-toggle-on"></i></a>
										<?php else:?>
											<a href="<?php echo wp_nonce_url( add_query_arg(array('action'=>'publish','job_id'=>get_the_ID())), 'job-manage-action' );?>" class="member-manage-action" data-toggle="tooltip" title="<?php esc_attr_e('Publish Job','noo')?>"><i class="fa fa-toggle-off"></i></a>
										<?php endif;?>
									<?php endif;?>

									<!-- myscript -->

									<?php if(Noo_Member::can_edit_job( $post->ID, get_current_user_id() ) && ($status !== 'canseledet') && ($status !== 'pending') ):?>
										<a href="https://работа-в-магнитогорске.рф/post-a-job/?job_refresh=true&job_id=<?php echo get_the_ID()?>&action=job_package#jform" class="member-manage-action action-refresh" data-toggle="tooltip" title="Обновить вакансию"><i class="fa fa-refresh"></i></a>
									<?php endif; ?>

									<!-- myscript -->

									<?php if(Noo_Member::can_edit_job( $post->ID, get_current_user_id() ) && ($status !== 'pending')):?>
										<?php if ($status !== 'canseledet') { ?>
										<a href="<?php echo Noo_Member::get_edit_job_url(get_the_ID())?>" class="member-manage-action" data-toggle="tooltip" title="<?php esc_attr_e('Edit Job','noo')?>"><i class="fa fa-pencil"></i></a>
										<?php } else {?>
										<a href="<?php echo Noo_Member::get_edit_job_url(get_the_ID())?>&job_update=full" class="member-manage-action" data-toggle="tooltip" title="<?php esc_attr_e('Edit Job','noo')?>"><i class="fa fa-pencil"></i></a>	
									<?php } endif; ?>
									<?php if( $status == 'expired' ) : ?>
										<?php //if (noo_get_post_meta( $post->ID, '_canseled' ) == '') ?>
								 		<a href="#" class="member-manage-action" data-toggle="tooltip" title="Срок Вакансии истёк"><i class="fa fa-clock-o"></i></a>
								 		
									<?php  endif;?>
									<a href="<?php echo wp_nonce_url( add_query_arg(array('action'=>'delete','job_id'=>get_the_ID())), 'job-manage-action' );?>" class="member-manage-action action-delete" data-toggle="tooltip" title="<?php esc_attr_e('Delete Job','noo')?>"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php endwhile;?>
					</tbody>
				</table>
			</div>
			<div class="member-manage-toolbar bottom-toolbar clearfix">
				<div class="bulk-actions clearfix pull-left">
					
					<div class="form-control-flat">
						<select name="action2">
							<option selected="selected" value="-1"><?php _e('-Bulk Actions-','noo')?></option>
							<?php foreach ($bulk_actions as $action=>$label):?>
							<option value="<?php echo esc_attr($action)?>"><?php echo esc_html($label)?></option>
							<?php endforeach;?>
						</select>
						<i class="fa fa-caret-down"></i>
					</div>
					<button type="submit" class="btn btn-primary"><?php _e('Go', 'noo')?></button>
				</div>
				<div class="member-manage-page pull-right">
					<?php noo_pagination(array(),$r)?>
				</div>
			</div>
		</form>
	<?php else:?>
		<h4><?php echo __("No jobs found; why not post one?",'noo')?></h4>
		<p>
			<a href="<?php echo Noo_Member::get_post_job_url(); ?>" class="btn btn-primary"><?php _e('Post Job', 'noo')?></a>
		</p>
	<?php endif;?>
</div>
<?php
do_action('noo_member_manage_job_after');
wp_reset_query();